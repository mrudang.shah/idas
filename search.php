<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package idas
 */

get_header('landing'); ?>
<div class="search-page">
	
	<?php 
	global $wp_query; 
	global $post;
	$query_string = esc_attr($query_string); 
	$blogs = get_sites( ['site__not_in' => [1,5] ] );
	$search_result = array();
	foreach ( $blogs as $blog ): 
		switch_to_blog($blog->blog_id);  
		$search = new WP_Query($query_string);
		if ($search->found_posts > 0) { 
			foreach ( $search->posts as $post ) { 
				setup_postdata($post);
				if((get_field('past_winners_display_options')=='content') || $post->post_type == 'judges_post' || $post->post_name == 'innovators' ): 
					$search_result[] = get_the_ID();
				endif; 
			} 
		} 
	endforeach; 
	restore_current_blog();
	
	if(!empty($search_result)){ ?>
	<div class="page-title">
		<div class="container">
			<div class="row">
				<div class="col span_6">
					<div class="inner-wrap">
						<h1 style="color: #19233b;">results for "<span><?php echo get_search_query(); ?></span>"</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="search-list">
		<div class="container main-content">
			<div class="row">
				<div class="col span_12">
					<div id="search-results" data-layout="list-no-sidebar">
						<?php 
						global $wp_query; 
						global $post;
						$query_string = esc_attr($query_string);
						$blogs = get_sites( ['site__not_in' => [1,5] ] );
						foreach ( $blogs as $blog ): 
							switch_to_blog($blog->blog_id);  
				    	$search = new WP_Query($query_string);
				    	if ($search->found_posts > 0) { 
							foreach ( $search->posts as $post ) { 
								setup_postdata($post); 
								
								
								if((get_field('past_winners_display_options')=='content') || $post->post_type == 'judges_post' || $post->post_name == 'innovators' ): ?> 
									
									<article id="post-<?php the_ID(); ?>" <?php post_class('search_data'); ?>>
										<div class="inner-wrap">
											<div class="pointed_list">
											<?php if(get_field('award_title') == 'Winner'){ ?>
												<div class="subtitle1"><?php echo get_field('award_title') ?></div>
											<?php } elseif($post->post_type == 'judges_post'){  ?>
												<div class="subtitle1">Judge</div>	
											<?php } elseif($post->post_name == 'innovators'){ ?>
												<div class="subtitle2">Innovator</div>	
											<?php } else{ ?>
												<div class="subtitle2"><?php echo get_field('award_title') ?></div>	<?php }
												$yearofpost = new DateTime(get_field('winner_year'));
												$postyear = $yearofpost->format('Y');
												echo "<span class=".$postyear.">",$postyear,"</span>"; 
												echo "<span>",get_bloginfo( 'name' ),"</span>";
												$jobid = get_the_ID();
												$queryterms = "
												SELECT *
												FROM ".$table_prefix."terms terms, ".$table_prefix."term_taxonomy term_taxonomy, ".$table_prefix."term_relationships term_relationships 
												WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id)
												AND term_relationships.object_id = ".get_the_ID()."
												";

												$terms = $wpdb->get_results($queryterms, OBJECT);
												if ( $terms != null ) {
													foreach( $terms as $term ) {
													echo "<span class='cat-name'>", $term -> name,"</span>" ;
													unset($term);
												} }  
											echo "</div>";
											if($post->post_name == 'innovators'){ ?>
											<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_field('innovator_name') ?></a></h2>
											<?php } else{ ?>	
											<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
											<?php } if($post->post_type == 'judges_post'){ ?>
											<div id="entry-content"><?php echo wp_trim_words(get_the_excerpt(), 20); ?> </div>
											<?php } elseif($post->post_name == 'innovators'){ ?>	
											<div id="entry-content"><?php echo innovator_excerpt(); ?> </div>
											<?php } else{ ?>
											<div id="entry-content"><?php echo custom_field_excerpt(); ?> </div>
											<?php } ?>	
										</div>
									</article>		
								<?php endif; 
							} 
						} 
						endforeach; 
						the_posts_navigation();
						restore_current_blog();
						?>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<?php } else { ?>
	<div class="page-title">
		<div class="container">
			<div class="row">
				<div class="col span_6">
					<div class="inner-wrap">
						<h1 style="color: #19233b;"><?php esc_html_e( 'nothing found for ', 'idas' ); ?>"<span><?php echo get_search_query(); ?></span>"</h1>
						<p style="text-align: center;"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'idas' ); ?></p>
					</div>
				</div>
			</div>
		</div>
		
	</div>	
	<?php } ?>
</div>

<?php get_footer('landing'); ?>
