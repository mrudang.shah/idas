<?php
/**
 * The template for displaying judges single posts
 */
get_header('landing'); 

while ( have_posts() ) : the_post();  
$judge_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
$blogid = get_current_blog_id();
if($blogid == 2){ $judges_classname = "ctcg-judges"; } 
elseif ($blogid == 3) { $judges_classname = "hcg-judges"; } 
elseif ($blogid == 4) { $judges_classname = "nycg-judges"; }

?>
<div class="meet-the-judges <?php echo $judges_classname ?>">
<div class="container">
	<div class="row white-block left" id="greg">
    	<div class="col-sm-12 col-xs-12 col-md-5 col">
        	<div class="matchHeight" style="height: 692px;">
                <div class="judge-photo judge1" style="background-image: url('<?php echo $judge_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;"></div>
            	</div>
          	</div>
          	<div class="col-sm-12 col-xs-12 col-md-7 col">
            	<div class="matchHeight">
                	<div class="content-inner">
                    	<div class="bio-top">
                        <span class="subtitle1"><?php the_field('singleSubtitle') ?></span>
                        <div class="name">
                          <?php the_field('single_firstjudge_firstname'); echo str_repeat('&nbsp;', 1);
                          the_field('single_firstjudge_lastname');
                        	if(get_field('single_secjudge_firstname')): ?> <br><span>&</span>
                        	<?php the_field('single_secjudge_firstname'); echo str_repeat('&nbsp;', 1);
                          the_field('single_secjudge_lastname'); endif; ?>
                        </div>
                        <div class="info">
                        	<?php if(get_field('single_website_name')): ?>
                          <a href="<?php the_field('single_website_url') ?>" target="_blank">
                          	<i class="fas fa-link"></i><?php the_field('single_website_name') ?>
                          </a>
                        <?php endif; if(get_field('single_instagram_name')): ?>
                          <a class="instagram" href="<?php the_field('single_instagram_url') ?>" target="_blank">
                          	<i class="fab fa-instagram"></i><?php the_field('single_instagram_name') ?>
                          </a>
                        <?php endif; ?>
                        </div>
                      </div>
                      <?php if(get_the_content()): the_content(); endif; ?>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>
<?php endwhile; get_footer('landing'); ?>