<?php
@ini_set(‘upload_max_size’ , ’512M’);
@ini_set(‘post_max_size’, ’512M’);
@ini_set(‘max_execution_time’, ‘3000’);

/**
 * idas functions and definitions
 *
 * @package idas
 */

if ( ! function_exists( 'idas_setup' ) ) :

	function idas_setup() {

		load_theme_textdomain( 'idas', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' ); // Add default posts and comments RSS feed links to head.
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'header-menu' => esc_html__( 'Header', 'idas' ),
			'footer-menu' => esc_html__( 'Footer', 'idas' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'idas_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function idas_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'idas_content_width', 640 );
}
add_action( 'after_setup_theme', 'idas_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function idas_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'idas' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'idas' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'idas_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function idas_scripts() {
	wp_enqueue_style('bootstrap', get_template_directory_uri().'/assets/css/bootstrap.css');
	wp_enqueue_style( 'idas-style', get_stylesheet_uri() );
	wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri(). '/assets/css/slick-theme.css' );
	wp_enqueue_style( 'slick', get_stylesheet_directory_uri(). '/assets/css/slick.css' );
	wp_enqueue_style( 'bootstrap-select', get_stylesheet_directory_uri(). '/assets/css/bootstrap-select.min.css' );
	wp_enqueue_style( 'awesome', get_stylesheet_directory_uri(). '/assets/css/font-awesome.css', true );
	wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.1.0/css/all.css');
	wp_enqueue_style( 'animate', get_stylesheet_directory_uri(). '/assets/css/animate.css' );
	wp_enqueue_style( 'fancybox', get_stylesheet_directory_uri(). '/assets/css/jquery.fancybox.min.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'cookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array( 'jquery' ), '', true );
	
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/assets/js/jquery.matchHeight-min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'bootstrap-select', get_template_directory_uri() . '/assets/js/bootstrap-select.min.js', array( 'jquery','bootstrap' ), '', true );
	wp_enqueue_script( 'isotop', get_template_directory_uri() . '/assets/js/isotope.pkgd.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/js/wow.min.js', array( 'jquery' ), '', true );
	
	wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery', ), '', true );
	if(get_current_blog_id()==1){
		wp_enqueue_script( 'home', get_template_directory_uri() . '/assets/js/home.js', array( 'jquery' ), '', true );
	}
}
add_action( 'wp_enqueue_scripts', 'idas_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


function idas_login_logo() { ?>
    <style type="text/css">
    #login h1 a, .login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png);
		height:100px;
		width:320px;
		background-size: 200px 100px;
    }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'idas_login_logo' );

/**
* Enable SVG Upload
*/
function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
* Create sub pages for the acf fields. 
*/
if( function_exists('acf_add_options_sub_page') ) {

	acf_add_options_sub_page('Header Settings');
	acf_add_options_sub_page('Footer Settings');

}


/**
* Disable CPT from main site that is not in used. 
*/
function specific_remove_menu_items() {
	
		$blogid = get_current_blog_id();
		if($blogid == 1){
			remove_menu_page( 'edit.php?post_type=past_winners_cpt' );
			remove_menu_page( 'edit.php?post_type=judges_post' );
		}

	
}
add_action( 'admin_menu', 'specific_remove_menu_items' );

/**
* Custom post type for Innovators
*/
function innovators_post_types() {
	$labels = array(
		'name'               => _x( 'Innovators', 'post type general name', 'idas' ),
		'singular_name'      => _x( 'Innovator', 'post type singular name', 'idas' ),
		'menu_name'          => _x( 'Innovators', 'admin menu', 'idas' ),
		'name_admin_bar'     => _x( 'Innovator', 'add new on admin bar', 'idas' ),
		'add_new'            => _x( 'Add New', 'innovator', 'idas' ),
		'add_new_item'       => __( 'Add New Innovator', 'idas' ),
		'new_item'           => __( 'New Innovator', 'idas' ),
		'edit_item'          => __( 'Edit Innovator', 'idas' ),
		'view_item'          => __( 'View Innovator', 'idas' ),
		'all_items'          => __( 'All Innovators', 'idas' ),
		'search_items'       => __( 'Search Innovators', 'idas' ),
		'parent_item_colon'  => __( 'Parent Innovators:', 'idas' ),
		'not_found'          => __( 'No Innovators found.', 'idas' ),
		'not_found_in_trash' => __( 'No Innovators found in Trash.', 'idas' )
	);

	$args = array(
		'labels'             => $labels,
    	'description'        => __( 'Description.', 'idas' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'menu_icon' 		 => 'dashicons-lightbulb',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'innovator' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'innovator', $args );
}
add_action( 'init', 'innovators_post_types', 0 );

/**
* Custom post type for Meet the Judges Section
*/

function meet_judges_sliders_post_types() {

	$labels = array(
		'name' => _x('Judges', 'Post Type General Name', 'idas'),
		'singular_name' => _x('Judge', 'Post Type Singular Name', 'idas'),
		'menu_name' => __('Judges', 'idas'),
		'name_admin_bar' => __('Judges', 'idas'),
		'archives' => __('Item Archives', 'idas'),
		'all_items' => __('All Judges', 'idas'),
		'add_new_item' => __('Add a Judge', 'idas'),
		'add_new' => __('Add a Judge', 'idas'),
		'new_item' => __('New Judge', 'idas'),
		'edit_item' => __('Edit a Judge', 'idas'),
		'update_item' => __('Update a Judge', 'idas'),
		'view_item' => __('View a Judge', 'idas'),
		'search_items' => __('Search Judges', 'idas'),
		'not_found' => __('Not found', 'idas'),
		'not_found_in_trash' => __('Not found in Trash', 'idas'),
		'insert_into_item' => __('Insert into item', 'idas'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'idas'),
		'items_list' => __('Meet the Judges list', 'idas'),
		'items_list_navigation' => __('Items list navigation', 'idas'),
		'filter_items_list' => __('Filter Meet the Judges list', 'idas'),
	);
	$args = array(
		'label' => __('Judges', 'idas'),
		'description' => __('You can add Judges', 'idas'),
		'labels' => $labels,
		'supports' => array('title','editor','author', 'revisions', 'thumbnail'),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-groups',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'rewrite'     => array('slug' => 'judge','with_front' => false),
		'capability_type' => 'page',
	);
	register_post_type('judges_post', $args);
}
add_action('init', 'meet_judges_sliders_post_types', 0);


/**
* Custom post type for Past Winners Section
*/

function past_winners_post_types() {

	$labels = array(
		'name' => _x('Winners', 'Post Type General Name', 'idas'),
		'singular_name' => _x('Winner', 'Post Type Singular Name', 'idas'),
		'menu_name' => __('Winners', 'idas'),
		'name_admin_bar' => __('Winners', 'idas'),
		'archives' => __('Item Archives', 'idas'),
		'all_items' => __('All Winners', 'idas'),
		'add_new_item' => __('Add New Winner', 'idas'),
		'add_new' => __('Add New Winner', 'idas'),
		'new_item' => __('New Winner', 'idas'),
		'edit_item' => __('Edit Winner', 'idas'),
		'update_item' => __('Update Winner', 'idas'),
		'view_item' => __('View Winner', 'idas'),
		'search_items' => __('Search Winner', 'idas'),
		'not_found' => __('Not found', 'idas'),
		'not_found_in_trash' => __('Not found in Trash', 'idas'),
		'insert_into_item' => __('Insert into item', 'idas'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'idas'),
		'items_list' => __('Past Winners list', 'idas'),
		'items_list_navigation' => __('Items list navigation', 'idas'),
		'filter_items_list' => __('Filter Winners list', 'idas'),
	);
	$args = array(
		'label' => __('Winners', 'idas'),
		'description' => __('You can add winners data', 'idas'),
		'labels' => $labels,
		'supports' => array('title', 'author', 'thumbnail'),
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-awards',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'rewrite'     => array('slug' => 'winner','with_front' => false),
		'capability_type' => 'page',
	);
	register_post_type('past_winners_cpt', $args);
}
add_action('init', 'past_winners_post_types', 0);



add_action( 'init', 'past_winners_taxonomies', 0 );
function past_winners_taxonomies() {
	$labels = array(
		'name'              => _x( 'Winners categories', 'taxonomy general name', 'idas' ),
		'singular_name'     => _x( 'Winners category', 'taxonomy singular name', 'idas' ),
		'search_items'      => __( 'Search winners categories', 'idas' ),
		'all_items'         => __( 'All winners categories', 'idas' ),
		'parent_item'       => __( 'Parent winners categories', 'idas' ),
		'parent_item_colon' => __( 'Parent winners categories:', 'idas' ),
		'edit_item'         => __( 'Edit winners categories', 'idas' ),
		'update_item'       => __( 'Update winners categories', 'idas' ),
		'add_new_item'      => __( 'Add New Winners Categories', 'idas' ),
		'new_item_name'     => __( 'New winners categories Name', 'idas' ),
		'menu_name'         => __( 'Winners Categories', 'idas' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'winners_categories' ),
	);

	register_taxonomy( 'winners_categories', array( 'past_winners_cpt' ), $args );
}

/**
* Short code for displaying a winners entries 
*/
function display_winners_entries(){ 
	ob_start(); 
	global $post;

	$year = new DateTime(get_field('display_post_of_the_year', $post->ID));
	$pageyear = $year->format('Y'); 
	
	if($post->post_parent)
  $childpages = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$post->post_parent."&echo=0");
  else
  $childpages = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$post->ID."&echo=0");

	$categories = get_terms( array(
		'taxonomy' => 'winners_categories',
		'order' => 'ASC',
		'hide_empty' => false,
		'parent'   => $parent_categories[0]->term_id,
	) );


	?>
		<div class="winners">
			<div class="container">

				<div class="button-group filter-button-group">
						<ul class="subpages" data-filter=".<?php echo $category->slug ?>">
							<?php echo $childpages ?>
						</ul>
				</div>

				<div class="button-group filter-button-group winner-group">
					<?php foreach($categories as $category){ ?>
					<a class="button <?php echo $category->slug ?>" href="#<?php echo $category->slug ?>" data-filter=".<?php echo $category->slug ?>">
						<?php echo $category->name ?>
					</a>
					<?php } ?>
				</div>
				<div class="grid row">
			<?php
			foreach($categories as $category){ 
				$queryargs = array(
				'post_type' => 'past_winners_cpt',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'winners_categories',
						'field'    => 'slug',
						'terms'    => $category->slug,
					),
				)
				);
				$the_query = new WP_Query( $queryargs );

				while ( $the_query->have_posts() ) : $the_query->the_post();
				
				$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
				$yearofpost = new DateTime(get_field('winner_year'));
				$postyear = $yearofpost->format('Y');

				if( $postyear == $pageyear ){
					if(get_field('past_winners_display_options') == 'content'):
				if(get_field('award_title') == 'Winner'){  ?>
			<a href='#' data-toggle='modal' data-target='#<?php echo $category->slug .'-'.get_the_ID() ?>'>
					<div class="grid-item singalImages <?php echo str_replace(" ","_",$category->slug);?>">
						<div class="img-cover">
							<div class="img" style="background-image: url('<?php echo $featured_img_url ?>')"></div>
							<div class="winners-content">
								<div class="subtitle1"><?php echo get_field('award_title') ?></div>
								<?php if(get_field('select_winner_names') == 'one'){ ?>
								<h2><?php	the_field('winner_name'); ?></h2> <?php } 
								elseif(get_field('select_winner_names') == 'two'){ ?>
								<h2><?php the_field('two_pst_winner_first_winner_name') ?> <br>
									<em><?php the_field('past_winner_preposition') ?></em>
									<?php echo str_repeat('&nbsp;', 0.50); the_field('second_past_winner_name')  ?>
								</h2><?php } 
								if(get_field('has_product_name') == 'yes' ){ 
									if(get_field('select_product_winner') == 'two'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('sec_product_first_winner_name') ?> 
									<em><?php the_field('product_name_preposition') ?></em>
									<?php the_field('product_second_winner_name') ?></span></h2>
								
								<?php } elseif(get_field('select_product_winner') == 'one'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('product_first_winner_name') ?></span></h2>
								<?php } }?>
							</div>
						</div>
					</div>
			</a>
			<?php } else{ ?>
			<a href='#' data-toggle='modal' data-target='#<?php echo $category->slug .'-'.get_the_ID() ?>'>
					<div class="grid-item twoColumn <?php echo str_replace(" ","_",$category->slug);?>">
						<div class="img-cover"><div class="img" style="background-image: url('<?php echo $featured_img_url ?>')"></div>
							<div class="winners-content">
								<div class="subtitle2"><?php echo get_field('award_title') ?></div>
								<?php if(get_field('select_winner_names') == 'one'){ ?>
								<h2><?php	the_field('winner_name'); ?></h2> <?php } 
								elseif(get_field('select_winner_names') == 'two'){ ?>
								<h2><?php the_field('two_pst_winner_first_winner_name') ?> <br>
									<em><?php the_field('past_winner_preposition') ?></em>
									<?php echo str_repeat('&nbsp;', 0.50); the_field('second_past_winner_name')  ?>
								</h2><?php } 
								if(get_field('has_product_name') == 'yes' ){ 
									if(get_field('select_product_winner') == 'two'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('sec_product_first_winner_name') ?> 
									<em><?php the_field('product_name_preposition') ?></em>
									<?php the_field('product_second_winner_name') ?></span></h2>
								
								<?php } elseif(get_field('select_product_winner') == 'one'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('product_first_winner_name') ?></span></h2>
								<?php } }?>
							</div>
						</div>
					</div>
			</a>
			<?php } endif; } endwhile; wp_reset_postdata();	} 
			foreach($categories as $category){ 
				$videoargs = array(
				'post_type' => 'past_winners_cpt',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'winners_categories',
						'field'    => 'slug',
						'terms'    => $category->slug,
					),
				)
				);
				$video_query = new WP_Query( $videoargs );
				while ( $video_query->have_posts() ) : $video_query->the_post();
					$videopostyear = new DateTime(get_field('winner_year'));
					$videoyear = $yearofpost->format('Y');
					if( $videoyear == $pageyear ){
				if(get_field('past_winners_display_options') == 'video'): ?>
			<a data-fancybox href="<?php echo get_field('past_winners_video_url') ?>" class="winner-vid">
       <div class="grid-item vid <?php echo $category->slug ?>">
          <div class="video_lightbox">
             <span class="play-button"><span class="inner-wrap"><i class="fas fa-play"></i></span></span>
             <div class="link-text">
                <span class="subtitle2"><?php echo get_field('past_winners_video_sub_title') ?></span>
                <h3><?php echo get_field('past_winners_video_title') ?></h3>
             </div>
          </div>
       </div>
    	</a>
    	<?php endif; } endwhile; wp_reset_postdata(); } ?>


				</div>
			</div>
		</div>
<?php 

return ob_get_clean();	
}
add_shortcode('past_winners_entries','display_winners_entries');

/*
 * Add columns to past winners post list
 */
function add_acf_columns ( $columns ) {
  return array_merge ( $columns, array ( 
  	'award_title' => __( 'Award' ),
    'winner_year' => __( 'Year' ),
  ) );
}
add_filter ( 'manage_past_winners_cpt_posts_columns', 'add_acf_columns' );

function exhibition_custom_column ( $column, $post_id ) {
  switch ( $column ) {
  	case 'award_title':
    echo get_field('award_title');
    break;
    case 'winner_year':
    $yearofpost = new DateTime(get_field('winner_year'));
		$postyear = $yearofpost->format('Y');
    echo $postyear;
    break;
  }
}
add_action ( 'manage_past_winners_cpt_posts_custom_column', 'exhibition_custom_column', 10, 2 );

function add_year_columns_before_date($defaults) {

 $new = array();
 $winner_year = $defaults['winner_year'];  // save the tags column
 $award_title = $defaults['award_title'];
 unset($defaults['winner_year']);   // remove it from the columns list
 unset($defaults['award_title']);

 foreach($defaults as $key=>$value) {
  if($key=='date') {  
  	$new['award_title'] = $award_title;
    $new['winner_year'] = $winner_year;  
  }    
  $new[$key]=$value;
	}
	return $new;
}
add_filter ( 'manage_past_winners_cpt_posts_columns', 'add_year_columns_before_date' );

/*
* Custom video Short code
*/
function add_video_shortcode($videoatts=[]){
$params = shortcode_atts( array(
		'title' => $videoatts['title'], 
    'subtitle' => $videoatts['subtitle'],
    'href'  =>  $videoatts['url'],
    'savefromurl' => $videoatts['savefromurl']
    ), $videoatts );

$blogid = get_current_blog_id();
$videothumb = "ctcg-vid";
if($blogid == 2){
	$videothumb = "ctcg-vid";
} elseif ($blogid == 3) {
	$videothumb = "hcg-vid";
} elseif ($blogid == 4) {
	$videothumb = "nycg-vid";
}


if(is_front_page()){
return '<div class="matchHeight">
		<a data-fancybox="" href="' . esc_attr($params['href']) . '">
			<div class="'.$videothumb.' vid-thumb">
				<div class="video_lightbox">
				<span class="play-button">
					<span class="inner-wrap">
						<i class="fa fa-play" aria-hidden="true"></i>
					</span>
				</span>
					<div class="link-text">
						<span class="subtitle2">' . esc_attr($params['subtitle']) . '</span>
						<h3>' . esc_attr($params['title']) . '</h3>
					</div>
				</div>
			</div>
		</a>
	</div> ';
}
else{
return '
	<a data-fancybox="" href="' . esc_attr($params['href']) . '">
		<div class="video_lightbox">
			<span class="play-button">
				<span class="inner-wrap"><i class="fa fa-play" aria-hidden="true"></i></span>
			</span>
			<div class="link-text">
				<span class="subtitle2">' . esc_attr($params['subtitle']) . '</span>
				<h3>' . esc_attr($params['title']) . '</h3>
			</div>
		</div>
	</a>';
}
}
add_shortcode('video','add_video_shortcode');

/**
* Short code for sponsors slider.
*/
function sponsor_slider(){ 
	ob_start(); 
	global $post; 

	$sponsorsargs = array(
		'post_type' => 'sponsors_cpt',
		'post_status' => 'publish',
		'posts_per_page' => -1
	);
	$sponsorquery = new WP_Query( $sponsorsargs );
	if ( $sponsorquery->have_posts() ) : ?>
	<div class="sponspr">
		<div class="logos-slider">
			<?php while ( $sponsorquery->have_posts() ) : $sponsorquery->the_post();  
			$sponsor_img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			?>
			<div class="brand-img">
				<a href="<?php echo get_field('sponsor_url') ?>" target="_blank">
					<img src="<?php echo $sponsor_img_url[0] ?>" alt="">
				</a> 
			</div>
		<?php endwhile; ?>
		</div>
	</div>

	<?php endif; return ob_get_clean();	
}
add_shortcode('sponsors','sponsor_slider');

/*
* Custom css for Background Images
*/
function my_custom_styles(){
	if(get_current_blog_id()== '1'):
	 if( have_rows('landing_page_innovators') ): while ( have_rows('landing_page_innovators') ) : the_row();
	 $i++; ?>
		<style type="text/css">
		.innovator<?php echo $i?>:before{ background: url('<?php the_sub_field('innovator_image')?>')}
		</style>
 	<?php endwhile; endif; endif;
 	
 	if(is_page_template('page-templates/template-landing.php')):
 	if(get_field('one_col_image')){ ?>
	 	<style type="text/css">
		.innovator1:before{ background: url('<?php the_field('one_col_image') ?>')}
		.innovator2:before{ background: url('<?php the_field('one_col_image') ?>')}
		.innovator3:before{ background: url('<?php the_field('one_col_image') ?>')}
		</style>

	<?php } if(get_field('featured_post_innovator_image')){  ?>
		<style type="text/css">
		.innovator1:before{ background: url('<?php the_field('featured_post_innovator_image') ?>')}
		.innovator2:before{ background: url('<?php the_field('featured_post_innovator_image') ?>')}
		.innovator3:before{ background: url('<?php the_field('featured_post_innovator_image') ?>')}
		</style>
 	<?php }

 	if( have_rows('judges_gallery_repeater') ): while ( have_rows('judges_gallery_repeater') ) : the_row(); 
 		$i++; ?>

 		<style type="text/css">
		.judge<?php echo $i?>:before{ background: url('<?php the_sub_field('judge_image')?>')}
		</style>

 	<?php endwhile;endif;

	endif;
}
add_action('wp_head', 'my_custom_styles', 100);

/**
* Search page result excerpt
*/
function custom_field_excerpt() {
	global $post;
	$text = get_field('winner_description'); 
	if ( '' != $text ) {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]&gt;', ']]&gt;', $text);
		$excerpt_length = 20; // 20 words
		$excerpt_more = apply_filters('excerpt_more', ' ' . '...');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('the_excerpt', $text);
}

function innovator_excerpt() {
	global $post;
	$text = get_field('innovator_description'); 
	if ( '' != $text ) {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]&gt;', ']]&gt;', $text);
		$excerpt_length = 20; // 20 words
		$excerpt_more = apply_filters('excerpt_more', ' ' . '...');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('the_excerpt', $text);
}

/**
* change search term URL slug 
*/
function wpb_change_search_url() {
  if ( is_search() && ! empty( $_GET['s'] ) ) {
      wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
      exit();
  }
}
add_action( 'template_redirect', 'wpb_change_search_url' );

/**
* Custom Search Queries
**/
function SearchFilter($query) {
	$post_type = $_GET['post_type'];
	if (!$post_type) {
	    $post_type = ['judges_post','past_winners_cpt','page'];
	}
	if ($query->is_search) {
	    $query->set('post_type', $post_type);
	};
	return $query;
}
add_filter('pre_get_posts','SearchFilter');
