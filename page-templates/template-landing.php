<?php 
/**
* Template Name: Home Page
*/
get_header('landing'); 
$blogid = get_current_blog_id();
$videothumb = "ctcg-vid";
if($blogid == 2){ $videothumb = "ctcg-vid"; $innovator = "innovator1"; 
} elseif ($blogid == 3) { $videothumb = "hcg-vid"; $innovator = "innovator2"; 
} elseif ($blogid == 4) { $videothumb = "nycg-vid"; $innovator = "innovator3"; }
$left_block_class = get_field('left_block_background');
$right_block_class = get_field('right_block_background');
$left_button_outline = get_field('left_button_outline');
$right_button_outline = get_field('right_button_outline');
$select_button_behavior = get_field('select_button_behavior');
?>

<div class="banner">
	<div class="container">
		<div class="banner-box row">
			<div class="col-sm-12 col-xs-12 col-md-5 col">
  			<div class="matchHeight">
  				<div class="content-inner">
				<div class="subtitle1"><?php if(get_field('banner_subtitle')) the_field('banner_subtitle') ?></div>
				<div class="copy">
					<?php if(get_field('banner_logo')): ?>
					<div class="banner-logo">
						<img src="<?php the_field('banner_logo') ?>" alt="">
					</div>
					<?php endif; if(get_field('submission_page_text')): ?>
					<a href="<?php the_field('submission_page_link') ?>">
						<span class="btn-medium outline gld"><?php the_field('submission_page_text') ?></span>
					</a> <?php endif; ?>
				</div>
			</div>
		</div>
			</div>
			<?php if(get_field('video_url')): ?>
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="matchHeight">
					<a data-fancybox href="<?php the_field('video_url') ?>">
					<div class="<?php echo $videothumb ?> vid-thumb">
						<div class="video_lightbox">
							<span class="play-button">
								<span class="inner-wrap"><i class="fas fa-play"></i></span>
							</span>
							<div class="link-text">
								<span class="subtitle2"><?php the_field('video_subtitle') ?></span>
								<h3><?php the_field('video_title') ?></h3>
							</div>
						</div>
					</div>
					</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="about-the-awards">
  <div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="awards-block <?php echo $left_block_class ?> matchHeight about-connecticut">
					<div class="content">
						<div class="subtitle1"><?php if(get_field('about_the_awards_subtitle')) the_field('about_the_awards_subtitle') ?></div>
						<?php if(get_field('white_block_contents')): ?>
						<div class="content-inner">
							<?php the_field('white_block_contents') ?>
						</div>
						<?php endif ?>
					</div>
				</div>
			</div>
			<?php /*<div class="col-sm-12 col-xs-12 col-md-5 col">
				<?php if(get_field('light_gold_block')): ?>
				<?php if(get_field('right_column_button_url')): ?><a href="<?php the_field('right_column_button_url') ?>" target="_blank"><?php endif; ?>
				<div class="matchHeight cta <?php echo $right_block_class ?>">
					<div class="copy">
					<div class="cta-tickets"></div>
					<?php the_field('light_gold_block')  ?>
					</div>
					<span class="<?php echo $select_button_behavior.' '.$right_button_outline ?>"><?php the_field('button_text') ?></span>
				</div>
				</a>
				<?php endif; ?>
			</div> */ ?>
			<div class="col-sm-12 col-xs-12 col-md-5 col ticket-slider" id="slick">
				<?php if( have_rows('slider_for_tickets_rep') ): 
				while ( have_rows('slider_for_tickets_rep') ) : the_row();
					$right_block_background = get_sub_field('right_block_background');
					$select_button_behavior = get_sub_field('select_button_behavior');
					$is_bg_img_color = get_sub_field('is_bg_img_color');
					$ticket_bg_image = get_sub_field('ticket_bg_image');
					if($is_bg_img_color == 'background'){ ?> ?>
				<a href="<?php echo get_sub_field('button_url') ?>">
					<div class="matchHeight cta <?php echo $right_block_background ?>">
						<div class="copy">
							<div class="cta-tickets"></div>
							<h4><?php echo get_sub_field('slider_title') ?></h4>
							<?php echo get_sub_field('ticket_description') ?>
						</div>
						<span class="<?php echo $select_button_behavior ?>"><?php echo get_sub_field('button_text') ?></span>
					</div>
				</a>
				<?php } elseif($is_bg_img_color == 'image'){ ?>
				<a href="<?php echo get_sub_field('button_url') ?>">
					<div class="matchHeight cta" style="background-image: url('<?php echo $ticket_bg_image ?>')">
						<div class="copy">
							<div class="cta-tickets"></div>
							<h4><?php echo get_sub_field('slider_title') ?></h4>
							<?php echo get_sub_field('ticket_description') ?>
						</div>
						<span class="<?php echo $select_button_behavior ?>"><?php echo get_sub_field('button_text') ?></span>
					</div>
				</a>
				<?php } endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="sponspr">
	<div class="logos-slider">
		<?php if( have_rows('sponsor_repeater') ): while ( have_rows('sponsor_repeater') ) : the_row(); ?>
		<div class="brand-img">
			<?php if(get_sub_field('sponsor_url')) { $sponsor_url = get_sub_field('sponsor_url'); } 
					else{ $sponsor_url = "javascript:void(0)"; } ?>
			<a href="<?php echo $sponsor_url ?>" target="_blank">
				<img src="<?php the_sub_field('sponsor_image') ?>" alt="sponsor_image">
			</a> 
		</div>
	<?php endwhile; endif ?>
	</div>
</div>
<div class="featured-post">
	<div class="container">
		<div class="row">
			<?php if(get_field('set_column')=='one-column'){ ?>
				<div class="col-sm-12 col-xs-12 col-md-12 col">
					<div class="<?php echo $innovator ?> innovator-single matchHeight">
						<a href="<?php the_field('one_col_innovator_page_link') ?>">
							<div class="content-inner">
								<?php if(get_field('one_col_subtitle')): ?>
								<div class="subtitle1"><?php the_field('one_col_subtitle') ?></div>
								<?php endif; if(get_field('')) ?>
								<div class="innovator-name">
									<?php the_field('one_col_name') ?>
								</div>
							</div>
						</a>
					</div>
				</div>
				<?php } else{ ?>

			<div class="col-sm-12 col-xs-12 col-md-8 col">
				<div class="<?php echo $innovator ?> innovator-single matchHeight">
					<a href="<?php the_field('featured_post_page_link') ?>">
						<div class="content-inner">
							<?php if(get_field('featured_post_subtitle')): ?>
							<div class="subtitle1"><?php the_field('featured_post_subtitle') ?></div>
							<?php endif; if(get_field('featured_post_innovator_name')): ?>
							<div class="innovator-name">
								<?php the_field('featured_post_innovator_name') ?>
							</div>
							<?php endif; ?>
						</div>
					</a>
				</div>
			</div>
			<?php if(get_field('select_advertisement_type') == 'is_image'){ ?>
			<div class="col-sm-12 col-xs-12 col-md-4 col">
				<div class="ad skyscraper matchHeight">
					<a href="<?php the_field('featured_post_ad_url') ?>" target="_blank">
						<img src="<?php the_field('featured_post_ad_image') ?>" alt="" />
					</a>
				</div>
			</div>
			<?php }else{ ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col">
				<div class="ad skyscraper matchHeight">
					<?php the_field('add_iframe_code') ?>
				</div>
			</div>
			<?php } }?>
		</div>
	</div>
</div>

<div class="gallery-section container" style="margin-bottom: 140px">
	<div class="white-block">
		<?php if(get_field('gallery_section_title')): ?>
		<h1><?php the_field('gallery_section_title') ?></h1>
		<?php endif; ?>
		<a class="read-more" href="<?php the_field('gallery_section_redirect_page') ?>"><img src="<?php echo get_field('read_more_image') ?>" alt=""></a>
		<?php //echo do_shortcode('[judges_slider]') ?>
		<div class="gallery-slider ctcg-judges">
			<?php if( have_rows('judges_gallery_repeater') ): while ( have_rows('judges_gallery_repeater') ) : the_row();
				$f_name = strtolower(get_sub_field('judge_first_name'));
				$l_name = strtolower(get_sub_field('judge_last_name'));
				$i++; ?>
			<a href="<?php echo site_url().'/judges/#'.$f_name?>">
			<div class="judge judge<?php echo $i ?>">
				<div class="content-inner">
					<div class="judge-name">
						<span class="first-name"><?php the_sub_field('judge_first_name') ?></span>
						<span class="last-name"><?php the_sub_field('judge_last_name') ?></span>
					</div>
					<?php if(get_sub_field('judge_sec_first_name')): ?>
					<div class="innovator-name2">
						<span class="first-name"><?php the_sub_field('judge_sec_first_name') ?></span>
						<span class="last-name"><?php the_sub_field('judge_sec_last_name') ?></span>
					</div>
					<?php endif; ?>
				</div>
			</div>
			</a>
		<?php endwhile; endif; ?>
		</div>
	</div>
</div>

<?php get_footer('landing') ?>