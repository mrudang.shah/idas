<?php 
/**
*Template Name: Submissions
*/
get_header('landing'); 
?>

<section class="entry-deadlines">
  <div class="container text-center">
    <?php if(get_field('title_section')): ?>
    <h1 class="title_section" style="margin-top: 70px"><?php the_field('title_section') ?></h1>
    <?php endif; ?>
    <div class="row">
      <?php if( have_rows('submission_deadline_repeater') ): 
        while ( have_rows('submission_deadline_repeater') ) : the_row();
          
          $submission_date = strtotime(get_sub_field('submission_date'));
          $date = new DateTime(get_sub_field('submission_date'));
          if(strtotime(date('Ymd')) >= $submission_date)  { ?>
        <div class="col-sm-4 col-xs-12 col">
        <div class="submission-deadline closed">
          <a href="<?php the_sub_field('submission_url') ?>" target="_blank" >
            <div class="content-inner">
              <div class="subtitle1"><?php the_sub_field('submission_subtitle') ?></div>
              <div class="submission-date">
                <div class="month"><?php echo $date->format('M') ?></div>
                <div class="day gold-text"><?php echo $date->format('j') ?></div>
                <div class="th-year">
                  <div class="th gold-text"><?php echo $date->format('S') ?></div>
                  <div class="year"><?php echo $date->format('Y') ?></div>
                </div>
              </div>
              <div class="btn-medium solid blk closed-btn"><?php the_sub_field('submission_button_text') ?></div>
            </div>
          </a>
        </div>
        </div>
        <?php }else{ ?>
        <div class="col-sm-4 col-xs-12 col">
        <div class="submission-deadline">
          <a href="<?php the_sub_field('submission_url') ?>" >
            <div class="content-inner">
              <div class="subtitle1"><?php the_sub_field('submission_subtitle') ?></div>
              <div class="submission-date">
                <div class="month"><?php echo $date->format('M') ?></div>
                <div class="day gold-text"><?php echo $date->format('j') ?></div>
                <div class="th-year">
                  <div class="th gold-text"><?php echo $date->format('S') ?></div>
                  <div class="year"><?php echo $date->format('Y') ?></div>
                </div>
              </div>
              <div class="btn-medium solid blk enter"><?php the_sub_field('submission_button_text') ?></div>
            </div>
          </a>
        </div>
        </div>
        <?php } endwhile; endif; ?>
      </div>
    </div>
  </div>
</section>

<section class="categories">
  <div class="container text-center">
    <h1 class="title_section"><?php the_field('category_title') ?></h1>
    <p class="sub-title"><?php the_field('category_sub_title') ?></p>
    <div class="row">
    	<?php if( have_rows('unboxed_categories_repeater') ): 
      	while ( have_rows('unboxed_categories_repeater') ) : the_row(); ?>
      <div class="col-sm-12 col-xs-12 col-md-3 col">
        <span class="subtitle1"><?php the_sub_field('category_name') ?></span>
      </div>
    	<?php endwhile; endif; ?>  
    </div>
    <div class="row categories-boxed">
      <?php if( have_rows('boxed_categories_repeater') ): 
      	while ( have_rows('boxed_categories_repeater') ) : the_row(); ?>
      <div class="col-sm-12 col-xs-12 col-md-4 col">
        <div class="category-box matchHeight">
          <div class="subtitle1"><?php the_sub_field('boxed_category_name') ?></div>
          <p><?php the_sub_field('boxed_category_description') ?></p>
        </div>
      </div>
      <?php endwhile; endif; ?>  
    </div>
  </div>
</section>

<section class="eligibility">
  <div class="container">
    <div class="white-block">
      <h1 class="title_section"><?php the_field('eligibility_title') ?></h1>
      <h4><?php the_field('eligibility_sub_title') ?></h4>
      
      <ul class="residential-project">
        <?php if( have_rows('eligibility_description_repeater') ): 
        while ( have_rows('eligibility_description_repeater') ) : the_row(); ?>
        <li class="matchHeight">
          <label class="btn btn-light-gold"><?php the_sub_field('eligibility_label') ?></label>
          <p><?php the_sub_field('eligibility_description') ?></p>
        </li>
      <?php endwhile; endif; ?>
      </ul>
      <?php if(get_field('bottom_row_title')): ?>
      <h4><?php the_field('bottom_row_title') ?></h4><?php endif; ?>
      <ul class="submission-rules">
        <?php if( have_rows('add_rules_repeater') ): 
        while ( have_rows('add_rules_repeater') ) : the_row(); 
          $rules_condition = get_sub_field('rules_condition'); ?>
        <li class="matchHeight <?php echo $rules_condition ?>">
          <p><?php the_sub_field('rules_description') ?></p>
          <?php if(get_sub_field('rules_extra_information')): ?>
          <span><?php the_sub_field('rules_extra_information') ?></span><?php endif; ?>
        </li>
        <?php endwhile; endif; ?>
      </ul>
    </div>
  </div>
</section>

<section class="two-column">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col">
        <div class="white-block">
          <?php if(get_field('left_column_steps_title')): ?>
          <h1><?php the_field('left_column_steps_title') ?></h1><?php endif; 
          if(get_field('left_column_steps_subtitle')): ?>
          <span><?php the_field('left_column_steps_subtitle') ?></span><?php endif; ?>
          <dl class="steps">
            <?php if( have_rows('add_steps_repeater') ): 
            while ( have_rows('add_steps_repeater') ) : the_row(); ?>
            <dt><?php the_sub_field('left_column_steps_data') ?></dt>
            <?php endwhile; endif; ?>
          </dl>
        </div>
      </div>
      <div class="col-sm-6 col">
        <div class="white-block">
          <?php if(get_field('right_column_steps_title')): ?>
          <h1><?php the_field('right_column_steps_title') ?></h1><?php endif; ?>
          <?php if( have_rows('deadlines_repeater') ): 
            while ( have_rows('deadlines_repeater') ) : the_row(); ?>
          <h4><?php the_sub_field('right_column_steps_title_rep') ?></h4>
          <?php the_sub_field('right_column_steps_des_rep') ?>
        <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="entry-deadlines submit-project">
  <div class="container text-center">
    <?php if(get_field('title_last_section')): ?>
      <h1 class="title_section"><?php the_field('title_last_section') ?></h1>
    <?php endif; ?>
    <div class="row">
      <?php if( have_rows('submission_deadline_repeater') ): 
        while ( have_rows('submission_deadline_repeater') ) : the_row();
          
          $submission_date = strtotime(get_sub_field('submission_date'));
          $date = new DateTime(get_sub_field('submission_date'));
          if(strtotime(date('Ymd')) >= $submission_date)  { ?>
        <div class="col-sm-4 col-xs-12 col">
        <div class="submission-deadline closed">
          <a href="<?php the_sub_field('submission_url') ?>" target="_blank" >
            <div class="content-inner">
              <div class="subtitle1"><?php the_sub_field('submission_subtitle') ?></div>
              <div class="submission-date">
                <div class="month"><?php echo $date->format('M') ?></div>
                <div class="day gold-text"><?php echo $date->format('j') ?></div>
                <div class="th-year">
                  <div class="th gold-text"><?php echo $date->format('S') ?></div>
                  <div class="year"><?php echo $date->format('Y') ?></div>
                </div>
              </div>
              <div class="btn-medium solid blk closed-btn"><?php the_sub_field('submission_button_text') ?></div>
            </div>
          </a>
        </div>
        </div>
        <?php }else{ ?>
        <div class="col-sm-4 col-xs-12 col">
        <div class="submission-deadline">
          <a href="<?php the_sub_field('submission_url') ?>" >
            <div class="content-inner">
              <div class="subtitle1"><?php the_sub_field('submission_subtitle') ?></div>
              <div class="submission-date">
                <div class="month"><?php echo $date->format('M') ?></div>
                <div class="day gold-text"><?php echo $date->format('j') ?></div>
                <div class="th-year">
                  <div class="th gold-text"><?php echo $date->format('S') ?></div>
                  <div class="year"><?php echo $date->format('Y') ?></div>
                </div>
              </div>
              <div class="btn-medium solid blk enter"><?php the_sub_field('submission_button_text') ?></div>
            </div>
          </a>
        </div>
        </div>
        <?php } endwhile; endif; ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer('landing'); ?>