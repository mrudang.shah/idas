<?php 
/**
* Template Name: Sub site template
*/
get_header('landing');

while ( have_posts() ) : the_post();
	the_content();
endwhile;


get_footer('landing') ?>