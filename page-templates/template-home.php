<?php
/**
* Template Name: Landing Page
*/
get_header();

$left_block_class = get_field('left_block_background');
$right_block_class = get_field('right_block_background');
$left_button_outline = get_field('left_button_outline');
$right_button_outline = get_field('right_button_outline');
$select_button_behavior = get_field('select_button_behavior');
?>
<div class="home-banner">
	<div class="logo-light">
		<a href="<?php echo site_url(); ?>">
		<?php $attachment_id = get_field('site_logo'); $size = 'full'; 
		$site_logo = wp_get_attachment_image_src( $attachment_id, $size ); ?>
		<img src="<?php echo $site_logo[0]; ?>" alt="IDAS Logo" width="300px">
		</a>
	</div>
	<?php if( have_rows('slider_banner_images') ):  ?>
	<div class="banner-slider" id="slick">
	<?php while ( have_rows('slider_banner_images') ) : the_row();
	 $image = get_sub_field('upload_slider_image'); ?>
	<div class="img scroll" style="background-image: url('<?php echo $image; ?>')"></div>
	<?php endwhile; ?>
	</div> <?php endif; ?>
	<div class="market-content row">
		<h2 class="title_section"><?php the_field('banner_text'); ?></h2>
		<div class="select-market">
			<div class="col-sm-12 col-xs-12 col-md-4 wow col">
				<div class="market-select connecticut">
					<a href="<?php the_field('connecticut_site_url') ?>">CONNECTICUT</a>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-4 wow col">
				<div class="market-select hamptons">
					<a href="<?php the_field('hamptons_site_url') ?>">HAMPTONS</a>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-4 wow col">
				<div class="market-select new-york">
					<a href="<?php the_field('new_york_site_url') ?>">NEW YORK</a>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="about-the-awards">
  <div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-md-7 col landing">
				<div class="awards-block <?php echo $left_block_class ?> matchHeight">
					<?php if(get_field('about_the_awards_subtitle')): ?>
					<div class="subtitle1"><?php the_field('about_the_awards_subtitle') ?></div>
					<?php endif; echo get_field('white_block_contents'); 
					if(get_field('left_column_button_text')): ?>
					<a href="<?php the_field('redirect_link_address') ?>">
						<span class="btn-medium <?php echo $left_button_outline ?>">
							<?php the_field('left_column_button_text') ?></span>
					</a><?php endif; ?>
				</div>
			</div>
			<?php /*<div class="col-sm-12 col-xs-12 col-md-5 col">
				<?php if(get_field('right_column_button_url')): ?><a href="<?php the_field('right_column_button_url') ?>" target="_blank"><?php endif; ?>
				<div class="matchHeight cta <?php echo $right_block_class ?>">
					<div class="copy">
						<div class="cta-tickets"></div>
						<?php echo get_field('light_gold_block') ?>
					</div>
					<span class="<?php echo $select_button_behavior.' '.$right_button_outline ?>">
						<?php echo get_field('button_text') ?></span>
				</div>
				</a>
			</div> */ ?>
			<div class="col-sm-12 col-xs-12 col-md-5 col ticket-slider" id="slick">
				<?php if( have_rows('slider_for_tickets_rep') ): 
				while ( have_rows('slider_for_tickets_rep') ) : the_row();
					$right_block_background = get_sub_field('right_block_background');
					$select_button_behavior = get_sub_field('select_button_behavior');
					$is_bg_img_color = get_sub_field('is_bg_img_color');
					$ticket_bg_image = get_sub_field('ticket_bg_image');
					if($is_bg_img_color == 'background'){ ?> ?>
				<a href="<?php echo get_sub_field('button_url') ?>">
					<div class="matchHeight cta <?php echo $right_block_background ?>">
						<div class="copy">
							<div class="cta-tickets"></div>
							<h4><?php echo get_sub_field('slider_title') ?></h4>
							<?php echo get_sub_field('ticket_description') ?>
						</div>
						<span class="<?php echo $select_button_behavior ?>"><?php echo get_sub_field('button_text') ?></span>
					</div>
				</a>
				<?php } elseif($is_bg_img_color == 'image'){ ?>
				<a href="<?php echo get_sub_field('button_url') ?>">
					<div class="matchHeight cta" style="background-image: url('<?php echo $ticket_bg_image ?>')">
						<div class="copy">
							<div class="cta-tickets"></div>
							<h4><?php echo get_sub_field('slider_title') ?></h4>
							<?php echo get_sub_field('ticket_description') ?>
						</div>
						<span class="<?php echo $select_button_behavior ?>"><?php echo get_sub_field('button_text') ?></span>
					</div>
				</a>
				<?php } endwhile; endif; ?>
			</div> 
		</div>
	</div>
</section>
<section class="innovators">
    	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-md-12 wow col">
				<h1><?php if(get_field('header_text')) the_field('header_text') ?></h1>
			</div>
		</div>
		<div class="row">
			<?php if( have_rows('landing_page_innovators') ): while ( have_rows('landing_page_innovators') ) : the_row(); $i++;?>
			<div class="col-sm-12 col-xs-12 col-md-4 wow col">
				<a href="<?php echo get_sub_field('innovator_url') ?>">
				<div class="matchHeight innovator<?php echo $i ?>">
					<div class="content-inner">
						<?php if(get_sub_field('innovator_subtitle')): ?>
						<div class="subtitle1">
							<?php the_sub_field('innovator_subtitle') ?>
						</div>
						<?php endif; if(get_sub_field('innovator_first_name')): ?>
						<div class="innovator-name">
							<span class="first-name"><?php the_sub_field('innovator_first_name') ?></span>
							<span class="last-name"><?php the_sub_field('innovator_last_name') ?></span>
						</div>
						<?php endif; if(get_sub_field('innovator_2_first_name')): ?>
						<div class="innovator-name2">
							<span class="first-name"><?php the_sub_field('innovator_2_first_name') ?></span>
							<span class="last-name"><?php the_sub_field('innovator_2_last_name') ?></span>
						</div>
						<?php endif; ?>
					</div>
				</div>
				</a>
			</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>
<?php get_footer() ?>