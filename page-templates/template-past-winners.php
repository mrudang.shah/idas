<?php 
/*
*Template Name: Past Winners
*/
get_header('landing'); 
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>

<div class="winners-banner scroll wow animated fadeIn" style="background-image: url('<?php echo $featured_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;">
	<?php if(get_field('past_winner_banner_title')) ?>
	<div class="container">
	   <h2 class="title_section"><?php the_field('past_winner_banner_title') ?></h2>
	</div>
</div>
<?php echo do_shortcode('[past_winners_entries]');

get_footer('landing'); ?>
