<?php 
/*
* Template Name: Innovator
*/
get_header('landing'); 
$innovator_block_background = get_field('innovator_block_background'); ?>

<div class="featuredPost">
	<div class="container">
		<?php
		global $post;
		if($post->post_parent)
			$childpages = wp_list_pages("sort_column=post_date&sort_order=desc&title_li=&child_of=".$post->post_parent."&echo=0");
		else
			$childpages = wp_list_pages("sort_column=post_date&sort_order=desc&title_li=&child_of=".$post->ID."&echo=0"); ?>
		<div class="row white-block innovator-bio">
			<?php if(get_field('innovator_year')){ ?>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col desktop">
				<?php echo "<div class='judges-lists innovators-lists' style='padding-left: 60px;text-align: left;'><ul class='subpages'>"; 
				echo $childpages;
				echo "</ul></div>"; ?>
				<div class="matchHeight">
					<div class="post">
						<?php
						$year = new DateTime(get_field('innovator_year'));
						$pageyear = $year->format('Y');
						$args = array(
						'post_type' => 'innovator',
						'post_status' => 'publish',
						'posts_per_page' => 1,
						);
						$innovator_query = new WP_Query( $args );
						if ( $innovator_query->have_posts() ): while ($innovator_query->have_posts()) : $innovator_query->the_post();
						$yearofpost = new DateTime(get_field('winner_year'));
						$postyear = $yearofpost->format('Y');
						if($postyear == $pageyear){
						if(get_field('innovator_title')): ?>
						<h1><?php the_field('innovator_title') ?></h1>
						<?php endif; if(get_field('innovator_name')): ?>
						<h1 class="name"><?php the_field('innovator_name') ?>
						<?php if(get_field('second_innovator_full_name')): ?>
							<br><em>&amp;</em><?php echo str_repeat('&nbsp;', 1); 
							the_field('second_innovator_full_name'); endif ?></h1>
						<?php endif; if(get_field('innovator_website_name') || get_field('innovator_instagram_name')):?>
						<div class="info">
							<a href="<?php the_field('innovator_website_url') ?>" target="_blank">
								<i class="fas fa-link"></i><?php the_field('innovator_website_name') ?></a>
							<a class="instagram" href="<?php the_field('innovator_instagram_url') ?>" target="_blank">
								<i class="fab fa-instagram"></i><?php the_field('innovator_instagram_name') ?></a>
						</div>
						<?php endif; 
						if(get_field('innovator_description')): the_field('innovator_description'); endif;  ?>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col col">
				<div class="matchHeight">
					<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
					<div class="innovator-photo" style="background-image: url('<?php echo $featured_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;"></div>
					<?php if(get_field('innovator_video_url')): ?>
						<a data-fancybox href="<?php the_field('innovator_video_url') ?>">
							<div class="video_lightbox">
								<span class="play-button">
									<span class="inner-wrap"><i class="fas fa-play"></i></span>
								</span>
								<div class="link-text">
									<span class="subtitle2"><?php the_field('innovator_video_subtitle	') ?></span>
									<h3><?php the_field('innovator_video_title') ?></h3>
								</div>
							</div>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col col mobile">
				<div class="matchHeight">
					<div class="post">
					<?php if(get_field('innovator_title')): ?>
						<h1><?php the_field('innovator_title') ?></h1>
						<?php endif; if(get_field('innovator_name')): ?>
						<h1 class="name"><?php the_field('innovator_name') ?></h1>
						<?php endif; if(get_field('innovator_website_name') || get_field('innovator_instagram_name')):?>
						<div class="info">
							<a href="<?php the_field('innovator_website_url') ?>" target="_blank">
							<i class="fas fa-link"></i><?php the_field('innovator_website_name') ?></a>
							<a class="instagram" href="<?php the_field('innovator_instagram_url') ?>" target="_blank">
							<i class="fab fa-instagram"></i><?php the_field('innovator_instagram_name') ?></a>
						</div>
					<?php endif; 
					if(get_field('innovator_description')): the_field('innovator_description'); endif;  ?>
					</div>
				</div>
			</div>
			<?php } endwhile; endif; wp_reset_postdata(); 
			} else{
				$args = array(
					'post_type' => 'innovator',
					'post_status' => 'publish',
					'posts_per_page' => 1,
				);
			?>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col desktop">
				<?php echo "<div class='judges-lists innovators-lists' style='padding-left: 60px;text-align: left;'><ul class='subpages'>"; 
				echo $childpages;
				echo "</ul></div>"; ?>
				<div class="matchHeight">
					<div class="post">
						<?php
						$year = new DateTime(get_field('innovator_year'));
						$pageyear = $year->format('Y');
						$args = array(
						'post_type' => 'innovator',
						'post_status' => 'publish',
						'posts_per_page' => 1,
						);
						$innovator_query = new WP_Query( $args );
						if ( $innovator_query->have_posts() ):
						while ($innovator_query->have_posts()) : $innovator_query->the_post(); ?>
						<?php if(get_field('innovator_title')): ?>
						<h1><?php the_field('innovator_title') ?></h1>
						<?php endif; if(get_field('innovator_name')): ?>
						<h1 class="name"><?php the_field('innovator_name') ?>
						<?php if(get_field('second_innovator_full_name')): ?>
						<br><em>&amp;</em><?php echo str_repeat('&nbsp;', 1); 
						the_field('second_innovator_full_name'); endif ?></h1>
						<?php endif; if(get_field('innovator_website_name') || get_field('innovator_instagram_name')):?>
						<div class="info">
							<a href="<?php the_field('innovator_website_url') ?>" target="_blank">
							<i class="fas fa-link"></i><?php the_field('innovator_website_name') ?></a>
							<a class="instagram" href="<?php the_field('innovator_instagram_url') ?>" target="_blank">
							<i class="fab fa-instagram"></i><?php the_field('innovator_instagram_name') ?></a>
						</div>
						<?php endif; 
						if(get_field('innovator_description')): the_field('innovator_description'); endif;  ?>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col col">
				<div class="matchHeight">
					<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
					<div class="innovator-photo" style="background-image: url('<?php echo $featured_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;"></div>
					<?php if(get_field('innovator_video_url')): ?>
						<a data-fancybox href="<?php the_field('innovator_video_url') ?>">
							<div class="video_lightbox">
								<span class="play-button">
									<span class="inner-wrap"><i class="fas fa-play"></i></span>
								</span>
								<div class="link-text">
									<span class="subtitle2"><?php the_field('innovator_video_subtitle	') ?></span>
									<h3><?php the_field('innovator_video_title') ?></h3>
								</div>
							</div>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 col-lg-6 col col mobile">
				<div class="matchHeight">
					<div class="post">
						<?php if(get_field('innovator_title')): ?>
									<h1><?php the_field('innovator_title') ?></h1>
									<?php endif; if(get_field('innovator_name')): ?>
									<h1 class="name"><?php the_field('innovator_name') ?></h1>
							<?php endif; if(get_field('innovator_website_name') || get_field('innovator_instagram_name')):?>
							<div class="info">
								<a href="<?php the_field('innovator_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('innovator_website_name') ?></a>
										<a class="instagram" href="<?php the_field('innovator_instagram_url') ?>" target="_blank">
											<i class="fab fa-instagram"></i><?php the_field('innovator_instagram_name') ?></a>
							</div>
							<?php endif; 
							if(get_field('innovator_description')): the_field('innovator_description'); endif;  ?>
					</div>
				</div>
			</div>
			<?php endwhile; endif; } ?>
		</div>
	</div>
</div>
<?php get_footer('landing') ?>