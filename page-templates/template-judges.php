<?php
/*
*Template Name: Judges
*/
get_header('landing'); 
$blogid = get_current_blog_id();
if($blogid == 2){ $judges_classname = "ctcg-judges"; } 
elseif ($blogid == 3) { $judges_classname = "hcg-judges"; } 
elseif ($blogid == 4) { $judges_classname = "nycg-judges"; }

?>

<div class="meet-the-judges <?php echo $judges_classname ?>">
	<div class="container">
		<?php 
			global $post;
			if($post->post_parent)
				$childpages = wp_list_pages("sort_column=post_date&sort_order=desc&title_li=&child_of=".$post->post_parent."&echo=0");
			else
				$childpages = wp_list_pages("sort_column=post_date&sort_order=desc&title_li=&child_of=".$post->ID."&echo=0");
			echo "<div class='judges-lists'><ul class='subpages'>";
			echo $childpages;
			echo "</ul></div>";
		?>
		<h2 class="title_section"><?php echo get_field('judge_banner_title') ?></h2>
		<?php

		$year = new DateTime(get_field('judges_year'));
	    $pageyear = $year->format('Y');

	    if(get_field('judges_year')){

		$args = array(
			'post_type' => 'judges_post',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		);
		$judge_query = new WP_Query( $args );
		if ( $judge_query->have_posts() ):
		while ($judge_query->have_posts()) : $judge_query->the_post();
		$first_name = strtolower(get_field('single_firstjudge_firstname'));
		$judge_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
		$yearofpost = new DateTime(get_field('winner_year'));
		$postyear = $yearofpost->format('Y');
		if($postyear == $pageyear){
		$i++; if(($i % 2) != 0) { ?>
		<div class="row white-block left" id="<?php echo $first_name ?>">
			<div class="col-sm-12 col-xs-12 col-md-5 col">
				<div class="matchHeight">
					<div class="judge-photo judge<?php echo $i ?>" style="background-image: url('<?php echo $judge_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;">
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="matchHeight">
					<div class="content-inner">
						<div class="bio-top">
							<?php if(get_field('singleSubtitle')): ?><span class="subtitle1"><?php the_field('singleSubtitle') ?></span><?php endif; ?>
							<div class="name">
								<?php the_field('single_firstjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_firstjudge_lastname');
								if(get_field('single_secjudge_firstname')): ?> <br><span>&</span>
								<?php the_field('single_secjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_secjudge_lastname'); endif; ?>
							</div>
							<div class="info">
								<?php if(get_field('single_website_name')): ?>
                <a href="<?php the_field('single_website_url') ?>" target="_blank">
                 	<i class="fas fa-link"></i><?php the_field('single_website_name') ?>
                </a>
                <?php endif; if(get_field('single_instagram_name')): ?>
                <a class="instagram" href="<?php the_field('single_instagram_url') ?>" target="_blank">
                	<i class="fab fa-instagram"></i><?php the_field('single_instagram_name') ?>
                </a>
                <?php endif; ?>
							</div>
						</div>
						<?php if(get_the_content()): the_content(); endif;  ?>
						</div>
				</div>
			</div>
		</div>
		<?php } else{ ?>
		<div class="row white-block right" id="<?php echo $first_name?>">
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="matchHeight">
					<div class="content-inner">
						<div class="bio-top">
							<?php if(get_field('singleSubtitle')): ?><span class="subtitle1"><?php the_field('singleSubtitle') ?></span><?php endif; ?>
							<div class="name">
								<?php the_field('single_firstjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_firstjudge_lastname');
								if(get_field('single_secjudge_firstname')): ?> <br><span>&</span>
								<?php the_field('single_secjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_secjudge_lastname'); endif; ?>
							</div>
							<div class="info">
								<?php if(get_field('single_website_name')): ?>
                <a href="<?php the_field('single_website_url') ?>" target="_blank">
                 	<i class="fas fa-link"></i><?php the_field('single_website_name') ?>
                </a>
                <?php endif; if(get_field('single_instagram_name')): ?>
                <a class="instagram" href="<?php the_field('single_instagram_url') ?>" target="_blank">
                	<i class="fab fa-instagram"></i><?php the_field('single_instagram_name') ?>
                </a>
                <?php endif; ?>
							</div>
						</div>
						<?php if(get_the_content()): the_content(); endif; ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-5 col">
				<div class="matchHeight">
					<div class="judge-photo judge<?php echo $i ?>" style="background-image: url('<?php echo $judge_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;"></div>
				</div>
			</div>
		</div>
		<?php } } endwhile; endif; wp_reset_postdata();

		}else{
	    	$args = array(
			'post_type' => 'judges_post',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		);
		$judge_query = new WP_Query( $args );
		if ( $judge_query->have_posts() ):
		while ($judge_query->have_posts()) : $judge_query->the_post();
		$first_name = strtolower(get_field('single_firstjudge_firstname'));
		$judge_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

		$i++; if(($i % 2) != 0) { 
		?>
		<div class="row white-block left" id="<?php echo $first_name ?>">
			<div class="col-sm-12 col-xs-12 col-md-5 col">
				<div class="matchHeight">
					<div class="judge-photo judge<?php echo $i ?>" style="background-image: url('<?php echo $judge_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;">
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="matchHeight">
					<div class="content-inner">
						<div class="bio-top">
							<?php if(get_field('singleSubtitle')): ?><span class="subtitle1"><?php the_field('singleSubtitle') ?></span><?php endif; ?>
							<div class="name">
								<?php the_field('single_firstjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_firstjudge_lastname');
								if(get_field('single_secjudge_firstname')): ?> <br><span>&</span>
								<?php the_field('single_secjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_secjudge_lastname'); endif; ?>
							</div>
							<div class="info">
								<?php if(get_field('single_website_name')): ?>
                <a href="<?php the_field('single_website_url') ?>" target="_blank">
                 	<i class="fas fa-link"></i><?php the_field('single_website_name') ?>
                </a>
                <?php endif; if(get_field('single_instagram_name')): ?>
                <a class="instagram" href="<?php the_field('single_instagram_url') ?>" target="_blank">
                	<i class="fab fa-instagram"></i><?php the_field('single_instagram_name') ?>
                </a>
                <?php endif; ?>
							</div>
						</div>
						<?php if(get_the_content()): the_content(); endif;  ?>
						</div>
				</div>
			</div>
		</div>
		<?php } else{ ?>
		<div class="row white-block right" id="<?php echo $first_name?>">
			<div class="col-sm-12 col-xs-12 col-md-7 col">
				<div class="matchHeight">
					<div class="content-inner">
						<div class="bio-top">
							<?php if(get_field('singleSubtitle')): ?><span class="subtitle1"><?php the_field('singleSubtitle') ?></span><?php endif; ?>
							<div class="name">
								<?php the_field('single_firstjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_firstjudge_lastname');
								if(get_field('single_secjudge_firstname')): ?> <br><span>&</span>
								<?php the_field('single_secjudge_firstname'); echo str_repeat('&nbsp;', 1);
								the_field('single_secjudge_lastname'); endif; ?>
							</div>
							<div class="info">
								<?php if(get_field('single_website_name')): ?>
                <a href="<?php the_field('single_website_url') ?>" target="_blank">
                 	<i class="fas fa-link"></i><?php the_field('single_website_name') ?>
                </a>
                <?php endif; if(get_field('single_instagram_name')): ?>
                <a class="instagram" href="<?php the_field('single_instagram_url') ?>" target="_blank">
                	<i class="fab fa-instagram"></i><?php the_field('single_instagram_name') ?>
                </a>
                <?php endif; ?>
							</div>
						</div>
						<?php if(get_the_content()): the_content(); endif; ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-5 col">
				<div class="matchHeight">
					<div class="judge-photo judge<?php echo $i ?>" style="background-image: url('<?php echo $judge_img_url ?>');background-size: cover; background-repeat: no-repeat; background-position: center top;"></div>
				</div>
			</div>
		</div>
		<?php }  endwhile; endif; wp_reset_postdata(); 

	    }
		
		?>
	</div>
</div>
<?php get_footer('landing') ?>