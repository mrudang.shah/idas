<?php
/**
 * The header for our theme
 *
 * @package idas
 */
?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name="format-detection" content="telephone=no">
	<?php wp_head(); ?>
	<!--[if lt IE 11]>
	<link rel='stylesheet' id='finepoint-ie-css'  href='css/ie.css' type='text/css' media='all' />
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<!--<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>-->
</head>
<body class="">
	<!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-17473669-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-17473669-3');
  </script>
	<?php if(is_front_page()): ?>
		<div class="bg-right scroll"></div>
		<div class="bg-left"></div>
	<?php endif;
	if(get_field('header_class_name')){
	$header_class_name = get_field('header_class_name');
} else{
	$header_class_name = "dark-header";
}
$search_query = get_search_query();
if($search_query){
	$header_class_name = "dark-header";
}
 ?>
<div class="page">
<header id="header" class="<?php echo $header_class_name ?>">
	<div class="container">
		<nav id="nav-main">
			<div class="navbar navbar-inverse">
				<div class="navbar-header">
					<div class="location-btn dropdown">
						<span class="locaton">LOCATION</span>
						<select class="selectpicker" data-width="auto" id="selectlocation" tabindex="-98">
						<?php 
							$subsites = get_sites(
								[
									'site__not_in' => [1,5,6,7,8,9]
								]
							);
							foreach( $subsites as $subsite ) {
							  $subsite_id = get_object_vars($subsite)["blog_id"];
							  $blog_details = get_blog_details($subsite_id); 
							  ?>
							  <option value="<?php echo get_blog_details($subsite_id)->siteurl ?>" <?php if(get_current_blog_id() == $subsite_id) { ?> selected="selected" <?php } ?>>
							  	<?php echo get_blog_details($subsite_id)->blogname ?>
							  </option>
						<?php } ?>
						</select>
					</div>
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<a id="logo" href="<?php echo network_home_url(); ?>">
					<?php $class_name = get_field('header_class_name');
					if($search_query){
						$class_name = "dark-header";
					}
					if($class_name == 'dark-header'){ ?>
						<img class="dark-logo" alt="C&amp;G IDAs" src="<?php echo get_field('dark_header_site_logo','options') ?>">
					<?php } elseif($class_name == 'light-header' || $class_name == 'winners-header'){ ?>
						<img class="light-logo" alt="C&amp;G IDAs" src="<?php echo get_field('light_header_site_logo','options') ?>">
					<?php } else{ ?>
						<img class="dark-logo" alt="C&amp;G IDAs" src="<?php echo get_field('dark_header_site_logo','options') ?>">
					<?php } ?>
				</a>
			   	<div class="navbar-collapse collapse">
			   		<?php wp_nav_menu( array(
						'container' 	=> '',
						'menu_class' 	=> 'nav navbar-nav',
						'theme_location' => 'header-menu',       
						'menu' => 'Header',
					) ); ?>
				</div>
				<?php if(get_field('display_search_box','options')=='yes'): ?>
				<div class="searchbox">
					<div><a href="#searchbox" class=""><i class="fa fa-search" aria-hidden="true"></i></a></div>
				</div> 
				<?php endif; ?>
			</div> 
		</nav>
	</div>
	<?php if(get_field('display_search_box','options')=='yes'): ?>
	<div id="search-outer">
		<div id="search">
			<div class="container" style="visibility: visible; height: 70px;">
			  <div id="search-box">
			  	<div class="inner-wrap">
				   	<div class="col span_12">
				      <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				      	<input type="text" name="s" value="" placeholder="Search">
				      	<!-- <input type="hidden" name="post_type[]" value="past_winners_cpt" />
				      	<input type="hidden" name="post_type[]" value="judges_post" /> -->
				      </form>
				      <span>Hit enter to search</span>			        
				    </div>
				</div>
			  </div>
			  <div id="close"><a href="#">
			  <span class="close-wrap"><span class="close-line close-line1"></span>
			  <span class="close-line close-line2"></span></span></a></div>
			</div>
		</div>	
	</div>
	<div class="search-bg"></div> 
	<?php endif; ?>
</header>
