<?php
/**
 * The template for displaying all winners single posts
 */
get_header('landing'); 

while ( have_posts() ) : the_post();  
$modal_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
?>

<div class="container">
	<div class="modal-body single-post">
		<div class="first-sec row hero" style="padding-bottom: 40px;">
			<div class="col-sm-12 col-xs-12 col-md-12 col">
				<div class="content photo matchHeight">
					<?php the_post_thumbnail( 'full' ); ?>
					<?php //echo wp_get_attachment_image(859,"full"); ?>
					<?php //print_r(wp_get_attachment_metadata(859)); ?>
					
					<?php // <img src="<?php echo $modal_img_url " alt="" > ?>
				</div>
				<div class="content copy matchHeight">
	       	<div class="content-inner">
	       		<?php if(get_field('award_title') == 'Winner'){ ?>
						<div class="subtitle1"><?php echo get_field('award_title') ?></div>
						<?php } else{  ?>
						<div class="subtitle2"><?php echo get_field('award_title') ?></div>	
						<?php } if(get_field('select_winner_names') == 'one'): ?>
							<h2><?php	the_field('winner_name'); ?></h2> 

						<?php endif; if(get_field('second_winner_name')): ?>
							<h2><?php the_field('two_pst_winner_first_winner_name') ?> <br><em><?php the_field('past_winner_preposition') ?></em><?php the_field('second_winner_name')  ?>
							</h2><?php endif;

							if(get_field('has_product_name') == 'yes' ){ 
								if(get_field('select_product_winner') == 'two'){ ?>
								<h2 class="product"><?php the_field('past_winner_prd_name') ?>
								<span><?php the_field('sec_product_first_winner_name') ?> 
								<em><?php the_field('product_name_preposition') ?></em>
								<?php the_field('product_second_winner_name') ?></span></h2>
							
							<?php } elseif(get_field('select_product_winner') == 'one'){ ?>
								<h2 class="product"><?php the_field('past_winner_prd_name') ?>
								<span><?php the_field('product_first_winner_name') ?></span></h2>
							<?php } } 

							if(get_field('select_winner_names') == 'one'){ ?>
              <div class="info">
              	<?php if(get_field('past_winners_website_url')): ?>
                <a href="<?php the_field('past_winners_website_url') ?>" target="_blank"><i class="fas fa-link"></i>Website</a>
                <?php endif; if(get_field('past_winners_houzz_url')): ?>
                <a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank"><i class="fab fa-houzz"></i>Houzz</a>
                <?php endif; if(get_field('first_winner_instagram_url')): ?>
                <a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a><?php endif; ?>
                <div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<input type="text" name="Element To Be Copied" id="copy-me" value="<?php echo get_permalink(get_the_ID()) ?>" style="display: none;"/>
											<a href="javascript:void(0);" id="copy-btn" class="clipboard" title="copy to clipboard"><i class="fas fa-link"></i></a>
										</nav>
									</div>
              </div>
            	
            	<?php } else{ ?>
		          <div class="info">
		          	<?php if(get_field('first_winner_website_name')): ?>
									<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
										<i class="fas fa-link"></i><?php the_field('first_winner_website_name') ?></a>
									<?php endif; if(get_field('first_winner_houzz_name')): ?>
									<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
										<i class="fab fa-houzz"></i><?php the_field('first_winner_houzz_name') ?></a>
									<?php endif; if(get_field('first_winner_instagram_name')): ?>
									<a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('first_winner_instagram_name') ?></a>
									
									<?php endif; if(get_field('second_winner_website_name')): ?>
									<a href="<?php the_field('second_winner_website_url') ?>" target="_blank">
										<i class="fas fa-link"></i><?php the_field('second_winner_website_name') ?></a>
										<?php endif; if(get_field('second_winner_houzz_name')): ?>
									<a class="instagram" href="<?php the_field('second_winner_houzz_url') ?>" target="_blank">
										<i class="fab fa-houzz"></i><?php the_field('second_winner_houzz_name') ?></a>
									<?php endif; if(get_field('second_winner_instagram_name')): ?>
									<a class="instagram" href="<?php the_field('second_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('second_winner_instagram_name') ?></a>

									<?php endif; if(get_field('select_product_winner') == 'one' && get_field('past_winners_website_url')):  ?>
									<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
										<i class="fas fa-link"></i>Website</a>
									<?php endif; if(get_field('select_product_winner')=='one' && get_field('past_winners_houzz_url')): ?>
										<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i>Houzz</a>
									<?php endif; if(get_field('select_product_winner')=='one' && get_field('product_first_winner_instagram_url')): ?>
									<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a>
									<?php endif; 

									if(get_field('product_first_winner_website_name')):  ?>
									<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
										<i class="fas fa-link"></i><?php the_field('product_first_winner_website_name') ?></a>
									<?php endif; if(get_field('product_first_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i><?php the_field('product_first_winner_houzz_name') ?></a>
									<?php endif;
									if(get_field('product_first_winner_instagram_url') && get_field('product_first_winner_houzz_name')): ?>
									<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_first_winner_houzz_name') ?></a>
									<?php endif; 


									if(get_field('product_second_winner_website_name')):  ?>
									<a href="<?php the_field('product_second_winner_website_url') ?>" target="_blank">
										<i class="fas fa-link"></i><?php the_field('product_second_winner_website_name') ?></a>
									<?php endif; if(get_field('product_second_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('product_second_winner_houzz_url') ?>" target="_blank">
									<i class="fab fa-houzz"></i><?php the_field('product_second_winner_houzz_name') ?></a>
									<?php endif; if(get_field('product_second_winner_instagrma_url')): ?>
									<a class="instagram" href="<?php the_field('product_second_winner_houzz_name') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_second_winner_instagram_name') ?></a>
									<?php endif; ?>
									<div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<input type="text" name="Element To Be Copied" id="copy-me" value="<?php echo get_permalink(get_the_ID()) ?>" style="display: none;"/>
											<a href="javascript:void(0);" id="copy-btn" class="clipboard" title="copy to clipboard"><i class="fas fa-link"></i></a>
										</nav>
									</div>
							</div>
						<?php } echo get_field('winner_description') ?>
					</div>
				</div>
			</div>
		</div>

			<?php if( have_rows('winner_portfolio') ): 
			while ( have_rows('winner_portfolio') ) : the_row(); 
			$portfolio_image1 = get_sub_field('portfolio_image');
			$portfolio_image = $portfolio_image1['url'];
			$portfolio_image_alt = $portfolio_image1['alt'];
			$portfolio_image_title = $portfolio_image1['title'];
			$i++; 
			if(get_sub_field('portfolio_image_description') || get_sub_field('portfolio_quote_judge') || get_sub_field('portfolio_quote')) {
			//if(($i % 2) != 0) :  
			if(get_sub_field('align_this_row_as') == 'left'):  ?>
			<div class="row left" style="padding-bottom: 40px;">
				<?php if($portfolio_image): ?>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="single-photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<?php endif; ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?php endif; if(get_sub_field('align_this_row_as') == 'right'): // else: ?>
			<div class="row right" style="padding-bottom: 40px;">
				<div class="col-sm-12 col-xs-12 col-md-4 col desktop">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); echo "<br>"; endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif; 
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
				<?php if($portfolio_image): ?>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="single-photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<?php endif; ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col mobile">
	      	<div class="content copy matchHeight">
	      		<div class="content-inner">
	        		<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?> ”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
	        	</div>
	        </div>
	      </div>
			</div>
			<?php endif; 
		} else{ ?>
				<div class="row bottom" style="padding-bottom: 40px;">
            <div class="col-sm-12 col-xs-12 col-md-12 col">
                <div class="content photo matchHeight">
                    <img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
                </div>
            </div>
        </div>
			<?php } endwhile; endif; ?>
		</div>
</div>
<?php endwhile; if(is_singular( 'past_winners_cpt' )): ?>

<script type="text/javascript">
	var copyBtn = jQuery("#copy-btn"),
	input = jQuery("#copy-me");

	function copyToClipboardFF(text) {
	  window.prompt ("Copy to clipboard: Ctrl C, Enter", text);
	}

	function copyToClipboard() {
	  var success = true,
	  range = document.createRange(),
	  selection;

	  if (window.clipboardData) {
	    window.clipboardData.setData("Text", input.val());        
	  } else {
	    // Create a temporary element off screen.
	    var tmpElem = jQuery('<div>');
	    tmpElem.css({
	      position: "absolute",
	      left:     "-1000px",
	      top:      "-1000px",
	    });
	    // Add the input value to the temp element.
	    tmpElem.text(input.val());
	    jQuery("body").append(tmpElem);
	    // Select temp element.
	    range.selectNodeContents(tmpElem.get(0));
	    selection = window.getSelection ();
	    selection.removeAllRanges ();
	    selection.addRange (range);
	    // Lets copy.
	    try { 
	      success = document.execCommand ("copy", false, null);
	    }
	    catch (e) {
	      copyToClipboardFF(input.val());
	    }
	    if (success) {
	      tmpElem.remove();
	    }
	  }
	}
	copyBtn.on('click', copyToClipboard);
</script>

<?php endif; get_footer('landing'); ?>