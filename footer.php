<?php
/**
 * The template for displaying the footer
 *
 * @package idas
 */
wp_footer();
?>
<div class="container" id="footer">
	<div class="row footer landing">
		<div style="margin-left: 0; display: none;" class="col-sm-12 col-xs-12 col">
			<div class="ad banner-ad">
				<a href="#">
					<img class="banner-desktop" src="<?php echo get_field('landing_banner_for_desktop','options') ?>" alt="middle_img">
				</a>
				<a href="#">
				<img class="banner-mobile" src="<?php echo get_field('landing_banner_for_mobile','options') ?>" alt="middle_img">
				</a>
			</div>
		</div>
		<div style="margin-left: 0;" class="col-sm-12 col-xs-12 col">
			<?php if(get_field('display_buzz_section','options')=='yes'): ?>
			<div class="buzz-sec">
				<a href="<?php the_field('buzz_button_url','options') ?>" target="_blank">
					<div class="buzz-banner">
					<div class="heading"><span><?php the_field('buzz_heading','options') ?></span>
						<?php the_field('buzz_sub_heading','options') ?></div>
					<div class="presented-by">Presented by <img src="<?php the_field('presented_by_image','options') ?>"></div>
					<div class="buzz-button"><?php the_field('buzz_button_text','options') ?></div>
					</div>
				</a>
			</div>
			<?php endif; ?>
			<div class="logo_img"><img src="<?php echo get_field('footer_logo','options') ?>" alt="logo"></div>
		
		<div class="social-icons">
			<a  target="_blank" href="<?php echo get_field('landing_twitter_url','options') ?>"><span class="social-icon"><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
			<a href="<?php echo get_field('landing_facebook_url	','options') ?>" target="_blank" ><span class="social-icon"><i class="fa fa-facebook-f" aria-hidden="true"></i></span></a>
			<a href="<?php echo get_field('landing_instagram_url','options') ?>" target="_blank"><span class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i></span></a>
			<a href="<?php echo get_field('landing_pinterest_url','options') ?>" target="_blank"><span class="social-icon"><i class="fa fa-pinterest" aria-hidden="true"></i></span></a>
		</div>
		<?php echo get_field('landing_copywrite_content','options') ?>
		<div class="footer-badge">
			<a href="http://cottages-gardens.com" target="_blank">
				<img style="width:100%; height:auto; max-width:250px;" src="https://cgidas.com/wp-content/uploads/2019/04/CG_Logo_K.svg">
			</a>
		</div>
		</div>
	</div>
</div>

</div>
</body>
</html>
