<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
<div class="home-banner">
    <div class="logo-light">
      <a href="http://idas.moonshotna.com">
        <img src="http://idas.moonshotna.com/wp-content/uploads/2018/07/logo-white.svg" alt="IDAS Logo" width="300px">
      </a>
    </div>
    <div class="img scroll" style="background-image: url('http://idas.moonshotna.com/wp-content/uploads/2018/07/SS3_Nautilus_PoolHouse.jpg');">
    </div>
</div>
<section class="search-page">
<div class="container">
	<div class="page-wrap d-flex flex-row align-items-center">
   	 	<div class="row justify-content-center">
    		<div class="col-md-12 text-center">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</div>
</section>

<?php get_footer(); ?>