<?php
/**
 * The template for displaying the footer
 *
 * @package idas
 */
wp_footer();
if(get_field('footer_class_name')){
	$footer_class_name = get_field('footer_class_name');
} else {
	$footer_class_name = "dark-footer";
}
$search_query = get_search_query();
if($search_query){
	$footer_class_name = "dark-footer";
}
?>

<footer id="footer" class="<?php if($footer_class_name == 'winners-footer' ){ ?>light-footer <?php }echo $footer_class_name ?>">
	<div class="shape-divider-wrap" data-front="" data-style="tilt_alt" data-position="top">
		<svg class="shape-divider" fill="#ffffff" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 10" preserveAspectRatio="none">
			<polygon points="100 10 100 0 -4 10"></polygon>
		</svg>
	</div>
	<?php if(get_field('display_buzz_section','options')=='yes'): ?>
		<div class="buzz-sec">
			<a href="<?php the_field('buzz_button_url','options') ?>">
				<div class="buzz-banner">
				<div class="heading"><span><?php the_field('buzz_heading','options') ?></span>
					<?php the_field('buzz_sub_heading','options') ?></div>
				<div class="presented-by">Presented by <img src="<?php the_field('buzz_presented_by_image','options') ?>"></div>
				<div class="buzz-button"><?php the_field('buzz_button_text','options') ?></div>
				</div>
			</a>
		</div>
	<?php endif; if( is_front_page() && $footer_class_name == 'dark-footer'){
	if(get_field('title_sponsor_logo','options')): ?>
	<div class="title-sponsor hover">
		<div class="container">
		<a href="<?php echo get_field('title_sponsor_link','options') ?>" target="_blank">
			<img src="<?php echo get_field('title_sponsor_logo','options') ?>" alt="">
		</a>
		</div>
	</div>
	<?php endif;  } ?>
	

	<?php if( !is_front_page() && !is_404() && (get_field('header_class_name') == 'winners-header' || $footer_class_name == 'light-footer' || $footer_class_name == 'dark-footer') ):  
	if(get_field('footer_advertisement_type','options') == 'image'): ?>
	<div class="ad banner-ad">
		<a href="<?php the_field('inner_page_adv_url','options') ?>" target="_blank">
			<img class="banner-desktop" src="<?php the_field('advertisement_image_for_desktop_view', 'options') ?>" alt="middle_img">
		</a>
		<a href="<?php echo get_field('inner_page_adv_url','options') ?>" target="_blank">
			<img class="banner-mobile" src="<?php echo get_field('advertisement_image_for_mobile_view','options') ?>" alt="middle_img">
		</a>
	</div>
	<?php endif; if(get_field('footer_advertisement_type','options') == 'iframe'): ?>
	<div class="ad banner-ad">
		<div class="banner-desktop"><?php the_field('footer_iframe_code_desk','option') ?></div>
		<div class="banner-mobile"><?php the_field('footer_iframe_code_mob','option') ?></div>
	</div>
	<?php endif; endif;?>


	<div class="footer-bottom <?php if(is_search()){ ?>search_page<?php } ?>">
		<div class="container">
			<div class="row" >
			<div class="logo-footer">
				<a href="<?php echo network_home_url(); ?>">
				<?php if($footer_class_name == 'light-footer'  ) { ?>
					<img src="<?php echo get_field('footer_light_logo','options') ?>" alt="C&G IDAs">
				<?php } elseif(get_field('header_class_name') == 'winners-header'){ ?>
					<img src="<?php echo get_field('footer_light_logo','options') ?>" alt="C&G IDAs">
				<?php }else{ ?>
					<img src="<?php echo get_site_url() ?>/wp-content/uploads/2018/06/logo.svg" alt="C&G IDAs">
				<?php } ?>
				</a>
			</div>
			<?php wp_nav_menu( array(
				'container' 	=> '',
				'menu_class' 	=> 'navbar-footer',     
				'menu' => 'Footer Menu',
			) ); ?>
			</div>
			<?php if(is_front_page()){ ?>
			<div class="row copy-social">
				<div class="col-sm-12 col-xs-12 col-md-6 col">
					<a href="http://cottages-gardens.com" target="_blank">
					
					<img class="badge-dark" style="width:100%; height:auto; max-width:200px; text-align: left !important; margin-top: 20px; margin-left: -30px;" src="https://cgidas.com/wp-content/uploads/2019/04/CG_Logo_K.svg"> 
					</a>
				</div>
				<div class="col-sm-12 col-xs-12 col-md-6 col">
					<div class="social-icons">
						<a  target="_blank" href="<?php echo get_field('twitter_link','options') ?>">
							<span class="social-icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
						</a>
						<a href="<?php echo get_field('facebook_link','options') ?>" target="_blank" >
							<span class="social-icon"><i class="fa fa-facebook-f" aria-hidden="true"></i></span>
						</a>
						<a href="<?php echo get_field('instagram_link','options') ?>" target="_blank">
							<span class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i></span>
						</a>
						<a href="<?php echo get_field('pinterest_link','options') ?>" target="_blank">
							<span class="social-icon"><i class="fa fa-pinterest" aria-hidden="true"></i></span>
						</a>
					</div>
				</div>
			</div>
			<?php } else{ ?>
				<div class="social-icons" style="text-align: center;">
					<a  target="_blank" href="<?php echo get_field('twitter_link','options') ?>">
						<span class="social-icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
					</a>
					<a href="<?php echo get_field('facebook_link','options') ?>" target="_blank" >
						<span class="social-icon"><i class="fa fa-facebook-f" aria-hidden="true"></i></span>
					</a>
					<a href="<?php echo get_field('instagram_link','options') ?>" target="_blank">
						<span class="social-icon"><i class="fa fa-instagram" aria-hidden="true"></i></span>
					</a>
					<a href="<?php echo get_field('pinterest_link','options') ?>" target="_blank">
						<span class="social-icon"><i class="fa fa-pinterest" aria-hidden="true"></i></span>
					</a>
				</div>
			<?php } ?>
			<div class="col-sm-12 col-xs-12 text-center">
				<?php if(is_front_page()){ ?>
				<p style="letter-spacing: 0 !important; color: #000 !important; opacity: 1 !important;">Presented by</p>
				<div>
				    <a href="http://cottages-gardens.com" target="_blank">
					<img src="<?php echo get_field('presented_by_image','options') ?>" alt="" style="width: 100%;
						height: auto; max-width: 200px; display: inline-block; margin-top: -20px; margin-bottom: 40px;">
					</a>
				</div>
				<?php } else{ ?>
				<p <?php if($footer_class_name == 'dark-footer'): ?> style="color: #878789 !important;" <?php endif; ?>>© DULCE DOMUM, LLC</p>
				<?php } ?>
			</div>
		</div>
	</div>  
</footer>

<?php global $post; 
if( is_page('winners') || ( is_page() && ($post->post_parent == 9 || $post->post_parent == 17 || $post->post_parent == 18) ) ): 

$categories = get_terms( array(
    'taxonomy' => 'winners_categories',
    'order' => 'ASC',
    'hide_empty' => false,
) );

foreach($categories as $category){ 
	$modalargs = array(
	'post_type' => 'past_winners_cpt',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'tax_query' => array(
		array(
			'taxonomy' => 'winners_categories',
			'field'    => 'slug',
			'terms'    => $category->slug,
		),
	)
	);
	$modal_query = new WP_Query( $modalargs );
	while ( $modal_query->have_posts() ) : $modal_query->the_post();
	$modal_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
	$modal_img_url_thumb = get_the_post_thumbnail_url(get_the_ID(),'thumbnail');
	if(get_field('award_title') == 'Winner'){
	?>

<div class="modal fade modal-block" id="<?php echo $category->slug .'-'.get_the_ID() ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $category->slug .'-'.get_the_ID() ?>" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<i class="fas fa-times"></i>
			</button>
		</div>
		<div class="modal-body">
			<div class="row hero">
				<div class="col-sm-12 col-xs-12 col-md-12 col">
					<div class="content photo matchHeight">
						<img src="<?php echo $modal_img_url ?>" alt="" >
					</div>
					<div class="content copy matchHeight">
		       	<div class="content-inner">
							<div class="subtitle1"><?php echo get_field('award_title') ?></div>
							<?php if(get_field('select_winner_names') == 'one'): ?>
								<h2><?php	the_field('winner_name'); ?></h2> 

							<?php endif; if(get_field('second_winner_name')): ?>
								<h2><?php the_field('two_pst_winner_first_winner_name') ?> <br><em><?php the_field('past_winner_preposition') ?></em><?php the_field('second_winner_name')  ?>
								</h2><?php endif;

								if(get_field('has_product_name') == 'yes' ){ 
									if(get_field('select_product_winner') == 'two'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('sec_product_first_winner_name') ?> 
									<em><?php the_field('product_name_preposition') ?></em>
									<?php the_field('product_second_winner_name') ?></span></h2>
								
								<?php } elseif(get_field('select_product_winner') == 'one'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('product_first_winner_name') ?></span></h2>
								<?php } } 

								if(get_field('select_winner_names') == 'one'){ ?>
                <div class="info">
                	<?php if(get_field('past_winners_website_url')): ?>
                  <a href="<?php the_field('past_winners_website_url') ?>" target="_blank"><i class="fas fa-link"></i>Website</a>
                  <?php endif; if(get_field('past_winners_houzz_url')): ?>
                  <a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank"><i class="fab fa-houzz"></i>Houzz</a>
                  <?php endif; if(get_field('first_winner_instagram_url')): ?>
                  <a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a><?php endif; ?>
                  <div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<?php $permaUrl = get_the_permalink(); ?>
											<input type="text" value="<?php echo esc_url_raw($permaUrl); ?>" style="display: none;"/>
											<a href="#" class="clipboard" title="clipboard" id="btnCopy" onClick="onButtonClick(this)"><i class="fas fa-link"></i></a>
										</nav>
									</div>
                </div>
              	
              	<?php } else{ ?>
			          <div class="info">
			          	<?php if(get_field('first_winner_website_name')): ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('first_winner_website_name') ?></a>
										<?php endif; if(get_field('first_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i><?php the_field('first_winner_houzz_name') ?></a>
										<?php endif; if(get_field('first_winner_instagram_name')): ?>
										<a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('first_winner_instagram_name') ?></a>
										
										<?php endif; if(get_field('second_winner_website_name')): ?>
										<a href="<?php the_field('second_winner_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('second_winner_website_name') ?></a>
											<?php endif; if(get_field('second_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('second_winner_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i><?php the_field('second_winner_houzz_name') ?></a>
										<?php endif; if(get_field('second_winner_instagram_name')): ?>
										<a class="instagram" href="<?php the_field('second_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('second_winner_instagram_name') ?></a>

										<?php endif; if(get_field('select_product_winner') == 'one' && get_field('past_winners_website_url')):  ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i>Website</a>
										<?php endif; if(get_field('select_product_winner')=='one' && get_field('past_winners_houzz_url')): ?>
											<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
												<i class="fab fa-houzz"></i>Houzz</a>
										<?php endif; if(get_field('select_product_winner')=='one' && get_field('product_first_winner_instagram_url')): ?>
										<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a>
										<?php endif; 


										if(get_field('product_first_winner_website_name')):  ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('product_first_winner_website_name') ?></a>
										<?php endif; if(get_field('product_first_winner_houzz_name')): ?>
											<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
												<i class="fab fa-houzz"></i><?php the_field('product_first_winner_houzz_name') ?></a>
										<?php endif;
										if(get_field('product_first_winner_instagram_url') && get_field('product_first_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_first_winner_houzz_name') ?></a>
										<?php endif; 


										if(get_field('product_second_winner_website_name')):  ?>
										<a href="<?php the_field('product_second_winner_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('product_second_winner_website_name') ?></a>
										<?php endif; if(get_field('product_second_winner_houzz_name')): ?>
											<a class="instagram" href="<?php the_field('product_second_winner_houzz_url') ?>" target="_blank">
										<i class="fab fa-houzz"></i><?php the_field('product_second_winner_houzz_name') ?></a>
										<?php endif; if(get_field('product_second_winner_instagrma_url')): ?>
										<a class="instagram" href="<?php the_field('product_second_winner_houzz_name') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_second_winner_instagram_name') ?></a>
										<?php endif; ?>
										<div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<?php $permaUrl = get_the_permalink(); ?>
											<input type="text" value="<?php echo esc_url_raw($permaUrl); ?>" style="display: none;"/>
											<a href="#" class="clipboard" title="copy to clipboard" id="btnCopy" onClick="onButtonClick(this)"><i class="fas fa-link"></i></a>
										</nav>
									</div>
								</div>
							<?php } echo get_field('winner_description') ?>
						</div>
					</div>
				</div>
			</div>

			<?php if( have_rows('winner_portfolio') ): 
			while ( have_rows('winner_portfolio') ) : the_row(); 
			$portfolio_image1 = get_sub_field('portfolio_image');
			$portfolio_image = $portfolio_image1['url'];
			$portfolio_image_alt = $portfolio_image1['alt'];
			$portfolio_image_title = $portfolio_image1['title'];
			$i++; 
			if(get_sub_field('portfolio_image_description') || get_sub_field('portfolio_quote_judge') || get_sub_field('portfolio_quote')) {
			//if(($i % 2) != 0) :  
			if(get_sub_field('align_this_row_as') == 'left'):  ?>
			<div class="row left">
				<?php if($portfolio_image): ?>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="content photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<?php endif; ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?php endif; if(get_sub_field('align_this_row_as') == 'right'): // else: ?>
			<div class="row right">
				<div class="col-sm-12 col-xs-12 col-md-4 col desktop">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); echo "<br>"; endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif; 
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
				<?php if($portfolio_image): ?>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="content photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<?php endif; ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col mobile">
	      	<div class="content copy matchHeight">
	      		<div class="content-inner">
	        		<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?> ”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
	        	</div>
	        </div>
	      </div>
			</div>
			<?php endif; 
		} else{ ?>
				<div class="row bottom">
            <div class="col-sm-12 col-xs-12 col-md-12 col">
                <div class="content photo matchHeight">
                    <img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
                </div>
            </div>
        </div>
			<?php } endwhile; endif; ?>
		</div>
	</div>
</div>
</div>
<?php }else{ ?>

<div class="modal fade zoomIn modal-block" id="<?php echo $category->slug .'-'.get_the_ID() ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $category->slug .'-'.get_the_ID() ?>" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
			</div>
			<div class="modal-body">
				<div class="row hero">
					<div class="col-sm-12 col-xs-12 col-md-12 col">
						<div class="content photo matchHeight">
							<img src="<?php echo $modal_img_url ?>" alt="">
						</div>
						<div class="content copy matchHeight">
							<div class="content-inner">
								<div class="subtitle2"><?php echo get_field('award_title') ?></div>
                
                <?php if(get_field('select_winner_names') == 'one'){ ?>
								<h2><?php	the_field('winner_name'); ?></h2> <?php } 
								else{ ?>

								<h2><?php the_field('two_pst_winner_first_winner_name') ?> 
									<em><?php the_field('past_winner_preposition') ?></em>
									<?php echo str_repeat('&nbsp;', 1); the_field('second_past_winner_name')  ?>
								</h2><?php }

								if(get_field('has_product_name') == 'yes' ){ 
									if(get_field('select_product_winner') == 'two'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('sec_product_first_winner_name') ?> 
									<em><?php the_field('product_name_preposition') ?></em>
									<?php the_field('product_second_winner_name') ?></span></h2>
								
								<?php } elseif(get_field('select_product_winner') == 'one'){ ?>
									<h2 class="product"><?php the_field('past_winner_prd_name') ?>
									<span><?php the_field('product_first_winner_name') ?></span></h2>
								<?php } } 
								
								if(get_field('select_winner_names') == 'one'){ ?>
                <div class="info">
                	<?php if(get_field('past_winners_website_url')): ?>
                  <a href="<?php the_field('past_winners_website_url') ?>" target="_blank"><i class="fas fa-link"></i>Website</a>
                  <?php endif; if(get_field('past_winners_houzz_url')): ?>
                  <a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank"><i class="fab fa-houzz"></i>Houzz</a>
                  <?php endif; if(get_field('first_winner_instagram_url')): ?>
                  <a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a><?php endif; ?>
                  <div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<?php $permaUrl = get_the_permalink(); ?>
											<input type="text" value="<?php echo esc_url_raw($permaUrl); ?>" style="display: none;"/>
											<a href="#" class="clipboard" title="copy to clipboard" id="btnCopy" onClick="onButtonClick(this)"><i class="fas fa-link"></i></a>
										</nav>
									</div>
                </div>
              	
              	<?php } else{ ?>
			          <div class="info">
			          	<?php if(get_field('first_winner_website_name')): ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('first_winner_website_name') ?></a>
										<?php endif; if(get_field('first_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i><?php the_field('first_winner_houzz_name') ?></a>
										<?php endif; if(get_field('first_winner_instagram_name')): ?>
										<a class="instagram" href="<?php the_field('first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('first_winner_instagram_name') ?></a>
										
										<?php endif; if(get_field('second_winner_website_name')): ?>
										<a href="<?php the_field('second_winner_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('second_winner_website_name') ?></a>
											<?php endif; if(get_field('second_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('second_winner_houzz_url') ?>" target="_blank">
											<i class="fab fa-houzz"></i><?php the_field('second_winner_houzz_name') ?></a>
										<?php endif; if(get_field('second_winner_instagram_name')): ?>
										<a class="instagram" href="<?php the_field('second_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('second_winner_instagram_name') ?></a>

										<?php endif; if(get_field('select_product_winner') == 'one' && get_field('past_winners_website_url')):  ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i>Website</a>
										<?php endif; if(get_field('select_product_winner')=='one' && get_field('past_winners_houzz_url')): ?>
											<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
												<i class="fab fa-houzz"></i>Houzz</a>
										<?php endif; if(get_field('select_product_winner')=='one' && get_field('product_first_winner_instagram_url')): ?>
										<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i>Instagram</a>
										<?php endif; if(get_field('product_first_winner_instagram_url') && get_field('product_first_winner_houzz_name')): ?>
										<a class="instagram" href="<?php the_field('product_first_winner_instagram_url') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_first_winner_houzz_name') ?></a>
										<?php endif; 

										if(get_field('product_first_winner_website_name')):  ?>
										<a href="<?php the_field('past_winners_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('product_first_winner_website_name') ?></a>
										<?php endif; if(get_field('product_first_winner_houzz_name')): ?>
											<a class="instagram" href="<?php the_field('past_winners_houzz_url') ?>" target="_blank">
												<i class="fab fa-houzz"></i><?php the_field('product_first_winner_houzz_name') ?></a>
										<?php endif;


										if(get_field('product_second_winner_website_name')):  ?>
										<a href="<?php the_field('product_second_winner_website_url') ?>" target="_blank">
											<i class="fas fa-link"></i><?php the_field('product_second_winner_website_name') ?></a>
										<?php endif; if(get_field('product_second_winner_houzz_name')): ?>
											<a class="instagram" href="<?php the_field('product_second_winner_houzz_url') ?>" target="_blank">
										<i class="fab fa-houzz"></i><?php the_field('product_second_winner_houzz_name') ?></a>
										<?php endif; if(get_field('product_second_winner_instagrma_url')): ?>
										<a class="instagram" href="<?php the_field('product_second_winner_houzz_name') ?>" target="_blank"><i class="fab fa-instagram"></i><?php the_field('product_second_winner_instagram_name') ?></a>
										<?php endif; ?>
										<div class="shareExpand">
										<div class="share-label">
											<i class="fas fa-share-square"></i><span>Share</span>
										</div>
										<nav>
											<a href="http://twitter.com/intent/tweet?text=IDAs- <?php the_title(); ?>&amp;url=<?php echo get_permalink(get_the_ID()); ?>" class="twitter" target="_blank"><i class="fab fa-twitter"></i></a>
											<a href="https://www.facebook.com/sharer?u=<?php echo get_permalink(get_the_ID());?>&t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
											<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(get_the_ID()); ?>&amp;media=<?php echo $modal_img_url?>&amp;description=<?php the_title(); ?>" class="pinterest" target="_blank"><i class="fab fa-pinterest"></i></a>
											<a href="mailto:?subject=IDAs-<?php echo get_the_title(get_the_ID()) ?>&amp;body=<?php echo get_permalink(get_the_ID()) ?>" class="email"><i class="far fa-envelope-open"></i></a>
											<?php $permaUrl = get_the_permalink(); ?>
											<input type="text" value="<?php echo esc_url_raw($permaUrl); ?>" style="display: none;"/>
											<a href="#" class="clipboard" title="copy to clipboard" id="btnCopy" onClick="onButtonClick(this)"><i class="fas fa-link"></i></a>
										</nav>
									</div>
								</div>
                <?php } echo get_field('winner_description') ?>
							</div>
						</div>
					</div>
				</div>

				<?php if( have_rows('winner_portfolio') ): 
				while ( have_rows('winner_portfolio') ) : the_row(); 
				$portfolio_image1 = get_sub_field('portfolio_image');
				$portfolio_image = $portfolio_image1['url'];
				$portfolio_image_alt = $portfolio_image1['alt'];
				$portfolio_image_title = $portfolio_image1['title'];
				$i++;
				if(get_sub_field('portfolio_image_description') || get_sub_field('portfolio_quote_judge') || get_sub_field('portfolio_quote')) {
				//if(($i % 2) == 0) : 
				if(get_sub_field('align_this_row_as') == 'left'):	?>

			<div class="row left">
				<?php if($portfolio_image): ?>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="content photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<?php endif; ?>
				<div class="col-sm-12 col-xs-12 col-md-4 col">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?php endif; if(get_sub_field('align_this_row_as') == 'right'):  ?>
			<div class="row right">
				<div class="col-sm-12 col-xs-12 col-md-4 col desktop">
					<div class="content copy matchHeight">
						<div class="content-inner">
							<p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif;
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?>”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-md-8 col">
					<div class="content photo matchHeight">
						<img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-md-4 col mobile">
            <div class="content copy matchHeight">
               <div class="content-inner">
                <p>
								<?php if(get_sub_field('portfolio_image_description')): 
									the_sub_field('portfolio_image_description'); endif;
									if(get_sub_field('portfolio_image_description') && get_sub_field('portfolio_quote')):
										echo "<br><br>";
									endif; 
									if(get_sub_field('portfolio_quote')): ?><em>“<?php the_sub_field('portfolio_quote'); ?> ”</em>
									<?php endif; if(get_sub_field('portfolio_quote_judge')): ?>
									<span class="author">— <?php the_sub_field('portfolio_quote_judge'); ?></span>
								<?php endif; ?>
								</p>
               </div>
            </div>
        </div>
			</div>
			<?php endif; } else { ?>
			<div class="row bottom">
        <div class="col-sm-12 col-xs-12 col-md-12 col">
            <div class="content photo matchHeight">
                <img src="<?php echo $portfolio_image; ?>" alt="<?php echo $portfolio_image_alt; ?>" title="<?php echo $portfolio_image_title; ?>">
            </div>
        </div>
      </div>
			<?php  } endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>

<?php } endwhile; wp_reset_postdata(); } endif; ?>

</div>
<?php if( is_page('winners') || ( is_page() && ($post->post_parent == 9 || $post->post_parent == 17 || $post->post_parent == 18) ) ):  ?>
<script>
			/*var txt = document.getElementById('xyz3');*/
			//var btn = document.getElementById('btnCopy');
			var txt;
			console.log(txt);
			var clipboard =
			{
				data      : '',
				intercept : false,
				hook      : function (evt)
				{
					if (clipboard.intercept)
					{
						evt.preventDefault();
						evt.clipboardData.setData('text/plain', clipboard.data);
						clipboard.intercept = false;
						clipboard.data      = '';
					}
				}
			};
			window.addEventListener('copy', clipboard.hook);
			
			function onButtonClick (param)
			{
				txt = jQuery(param).prev();
				clipboard.data = txt.val();
				if (window.clipboardData)
				{
					window.clipboardData.setData('Text', clipboard.data);
				}
				else
				{
					clipboard.intercept = true;
					document.execCommand('copy');
				}
			}
		</script>

<?php endif; ?>
</body>
</html>
