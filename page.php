<?php
/**
 * The template for displaying all pages
 *
 * @package idas
 */

get_header();
?>

	<section class="about-the-awards">
       	<div class="container">
			<div class="row">
				<?php 
				while ( have_posts() ) :
					the_post();
				endwhile; // End of the loop.

				 ?>
			</div>
		</div>
	</section>

<?php
get_footer();