jQuery( document ).ready(function($) {
	simpleParallax();
});
function simpleParallax() {
	var scrolled = jQuery(window).scrollTop() + 1;
	jQuery('.scroll').css('background-position', '0px ' + (scrolled * 0.2) + 'px');
}
jQuery(window).scroll(function (e) {
	simpleParallax();
});