jQuery( document ).ready(function($) {
	new WOW().init();
	$('.matchHeight').matchHeight({
		property: 'height'
	});
	$(".icon").click(function(){
		$("body").toggleClass("open-model");
	});
	$("#overlay").click(function(){
		$("body").toggleClass("open-model");
	});
	simpleParallax();
});
jQuery('.carousel-block').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
});
jQuery(".button-group .btn").on('click',function($){
	$.cookie("category_change", $(this).text());
});

function simpleParallax() {
		var scrolled = jQuery(window).scrollTop() + 1;
		jQuery('.scroll').css('background-position', '0px ' + (scrolled * 0.2) + 'px');
	}
		jQuery(window).scroll(function (e) {
		simpleParallax();
	});
	/*jQuery('.innovators-blk').draggable({
		start: function(event, ui) {
			var tog = $(this).attr("data-class");
			jQuery("." + tog).toggleClass('open');
		}
	});*/
jQuery('.innovators-blk').click(function(event) {
		if ($(log).hasClass('open')) {
				var tog = $(this).attr("data-class");
				$("." + tog).toggleClass('open');
		}
		else {
				// actual click event code
		}
});


jQuery(document).ready(function(){
	jQuery(".selectpicker").change(function () {
		console.log("hello");
		jQuery.cookie("category_change", jQuery(this).find("option:selected").text());
		location.href = jQuery(this).val();
	});

	/*jQuery('.selectpicker option').map(function () {
		var category_change = jQuery.cookie("category_change");
		if( typeof category_change != 'undefined' && category_change ){
			category_change = category_change.toLowerCase();
			if (jQuery(this).text().toLowerCase() == category_change) return this;
		}
	}).attr('selected', 'selected');*/

	jQuery('.gallery-slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 600,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  autoplay:true,
	  autoplaySpeed:2500,
	  cssEase: 'ease',
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 991,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});

	jQuery('.logos-slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 600,
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  autoplay:true,
	  autoplaySpeed:2500,
	  cssEase: 'ease',
	  responsive: [
	    {
		  breakpoint: 1424,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});

	

});

/*jQuery(document).ready(function(){
	var right_value = jQuery(".featured-post .container").offset().left;
	jQuery(".white-block .img").css("margin-left","-"+ right_value +"px");
	jQuery(".white-block .img").css("padding-left", right_value + "px");
});

jQuery(window).resize(function(){
	var right_value = jQuery(".featured-post .container").offset().left;
	jQuery(".white-block .img").css("margin-left","-"+ right_value +"px");
	jQuery(".white-block .img").css("padding-left", right_value + "px");
}); */

jQuery(document).ready(function(){
	var $grid = jQuery('.grid').isotope({
	  itemSelector: '.grid-item',
	  stagger: 30,
	  filter :'.architecture'
	});

	jQuery('.filter-button-group').on( 'click', '.button', function() {
	  var filterValue = jQuery(this).attr('data-filter');
	  $grid.isotope({ filter: filterValue });
	});

	// change is-checked class on buttons
	jQuery('.button-group').each( function( i, buttonGroup ) {
	  var $buttonGroup = jQuery( buttonGroup );
	  $buttonGroup.on( 'click', 'button', function() {
		$buttonGroup.find('.is-checked').removeClass('is-checked');
		jQuery( this ).addClass('is-checked');
	  });
	});
	function simpleParallax() {
		var scrolled = jQuery(window).scrollTop() + 1;
		jQuery('.scroll').css('background-position', '0px ' + (scrolled * 0.2) + 'px');
	}
	//Everytime we scroll, it will fire the function
	jQuery(window).scroll(function (e) {
		simpleParallax();
	});
});

/*function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

jQuery(window).load(function() {
	var x = getCookie('category_change');
	jQuery('select[id=selectlocation]').val(x);
	jQuery('.selectpicker').selectpicker('refresh');
});*/