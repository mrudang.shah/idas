jQuery( document ).ready(function() {
	new WOW().init();

	jQuery('.matchHeight').matchHeight({
		property: 'height'
	});
	jQuery(".icon").click(function(){
		jQuery("body").toggleClass("open-model");
	});
	jQuery("#overlay").click(function(){
		jQuery("body").toggleClass("open-model");
	});

	jQuery('.about-the-awards .ticket-slider').slick({
		autoplay: true,
		autoplaySpeed: 9000,
		dots: true,
	  	infinite: true,
	  	speed: 1000,
	  	slidesToShow: 1,
	  	cssEase: 'linear'
	});

	jQuery('.home-banner .banner-slider').slick({
		autoplay: true,
		autoplaySpeed: 9000,
		dots: true,
  		infinite: true,
  		speed: 1000,
  		slidesToShow: 1,
  		cssEase: 'linear'
	});
	
});
jQuery('.carousel-block').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
});
jQuery(".button-group .btn").on('click',function($){
	$.cookie("category_change", $(this).text());
});

function simpleParallax() {
	var scrolled = jQuery(window).scrollTop() + 1;
	jQuery('.scroll').css('background-position', '0' + -(scrolled * 0.1) + 'px');
}
jQuery(window).scroll(function (e) {
	simpleParallax();
});
	/*jQuery('.innovators-blk').draggable({
		start: function(event, ui) {
			var tog = $(this).attr("data-class");
			jQuery("." + tog).toggleClass('open');
		}
	});*/
jQuery('.innovators-blk').click(function(event) {
		if ($(log).hasClass('open')) {
				var tog = $(this).attr("data-class");
				$("." + tog).toggleClass('open');
		}
		else {
				// actual click event code
		}
});


jQuery(document).ready(function(){
	jQuery(".selectpicker").change(function () {
		console.log("hello");
		jQuery.cookie("category_change", jQuery(this).find("option:selected").text());
		location.href = jQuery(this).val();
	});

	jQuery('.gallery-slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 600,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  autoplay:true,
	  autoplaySpeed:2500,
	  cssEase: 'ease',
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 991,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});

	jQuery('.logos-slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 600,
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  autoplay:true,
	  autoplaySpeed:2500,
	  cssEase: 'ease',
	  responsive: [
	    {
		  breakpoint: 1424,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});

	

});


jQuery(document).ready(function(){
	
	var hash = location.hash.substr(1);
	var flg = 1;
	if(hash==''){
		hash="architecture";
		flg = 0;
	}
	
	if(flg == 1){
		jQuery( ".winner-group a" ).each(function() {
			if (!(jQuery(this).hasClass(hash))) {
				//jQuery(this).remove();
			}else{
				jQuery(this).addClass("is-checked");
			}
		});
	}
	
	var $grid = jQuery('.grid').isotope({
	  itemSelector: '.grid-item',
	  stagger: 30,
	  filter :'.'+hash
	});

	jQuery('.filter-button-group').on( 'click', '.button', function() {
	  var filterValue = jQuery(this).attr('data-filter');
	  $grid.isotope({ filter: filterValue });
	});

	// change is-checked class on buttons
	jQuery('.button-group').each( function( i, buttonGroup ) {
	  var $buttonGroup = jQuery( buttonGroup );
	  $buttonGroup.on( 'click', 'a', function() {
		$buttonGroup.find('.is-checked').removeClass('is-checked');
		jQuery( this ).addClass('is-checked');
	  });
	});

	jQuery('.navbar .searchbox').click(function(){
		jQuery('#search-outer').addClass('material-open');
		jQuery('.search-bg').addClass('active');
		jQuery(this).addClass('active');
		jQuery('#header').addClass('active');
	});
	jQuery('#search-outer #search #close').click(function(){
		jQuery('#search-outer').removeClass('material-open');
		jQuery('.search-bg').removeClass('active');
		jQuery('.navbar .searchbox').removeClass('active');
		jQuery('#header').removeClass('active');
	});
});
