<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package idas
 */

get_header('landing');
?>

<div class="page-wrap d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
              <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/404-Error.png" alt="">
            </div>
            <div class="col-md-12 text-center" style="padding-bottom: 20px;">
            	<a href="<?php echo site_url() ?>" class="btn btn-primary btn-lg" style="background-color: #101014 !important;color: #fff !important;padding: 10px 15px !important;line-height: 1;">Back to Home</a>
          	</div>
        </div>
    </div>
</div>

<?php
get_footer('landing');